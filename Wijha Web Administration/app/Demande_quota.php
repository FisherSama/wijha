<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demande_quota extends Model
{
    //
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function user_Abonne()
    {
        return $this->belongsTo('App\UserAbon');
    }



    public function getAll_demandeQuotas($start,$limit){
    	$List_demandeQuotas = Demande_quota::join('user_abons', 'demande_quotas.user_abons_id', '=', 'user_abons.id')
                          ->join('users', 'user_abons.user_id', '=', 'users.id')
                         ->offset($start)
                         ->limit($limit)
                         ->select('*','users.id as id_user','demande_quotas.id as id')
                         ->get();
            return $List_demandeQuotas;
    }

    public function getAll_demandeQuotas_filtred($search,$start,$limit)  {
    	$List_demandeQuotas = Demande_quota::join('user_abons', 'demande_quotas.user_abons_id', '=', 'user_abons.id')
                             ->join('users', 'user_abons.user_id', '=', 'users.id')
                             ->where('users.nom','LIKE',"%{$search}%")
                             ->orWhere('users.prenom', 'LIKE',"%{$search}%")
                             ->orWhere('users.organisme', 'LIKE',"%{$search}%")
                             ->orWhere('users.num_telephone', 'LIKE',"%{$search}%")
                             ->orWhere('users.email', 'LIKE',"%{$search}%")
                             ->offset($start)
                             ->limit($limit)
                             ->select('*','users.id as id_user','demande_quotas.id as id')
                             ->get();

        return $List_demandeQuotas;                     
    }
    
    function construct_data($posts){
    	$data = array();
    	foreach ($posts as $post)
            {

                $nestedData['code_demande'] = '#'.$post->id;
                $nestedData['nom'] = $post->nom;
                $nestedData['organisme'] = $post->organisme;
                $nestedData['num_telephone'] = $post->num_telephone ;

                if ($post->etat_quota =="validé") {
                	$etat = '<span class="badge badge-success">validé</span>';

                }
                elseif($post->etat_quota =="En attente"){
                    $etat = '<span class="badge badge-warning">En attente</span>';
                }else{
                	$etat = '<span class="badge badge-danger">Refusé</span>';
                }

                $nestedData['etat'] = $etat;
                $nestedData['date'] =   date('d/m/Y h:i a',strtotime($post->created_at));
                $nestedData['option'] =  '<a role="button" class="btn btn-default btn-sm" data-toggle="modal"
                data-id="'.$post->id.'" data-nom="'.$post->nom.'" data-organisme="'.$post->organisme.'" 
                data-nbrUser="'.$post->nbr_user.'" data-nbrAffich="'.$post->nbr_afficheur.'"
                data-nbrEspace="'.$post->espace_demander.'" id="moreInfo"
                data-nbrLicence="'.$post->nbr_licence.'" data-etatDemande="'.$post->etat_quota.'"
                
                data-target="#moreInfoModal"><i class="entypo-info-circled"></i></a>';
                $data[] = $nestedData;

            }
            return $data;
    }


    public static function getDemandeuQuotas_ByUserAbonne($user_Abonne) {
        $List_demandeQuotas = Demande_quota::join('user_abons', 'demande_quotas.user_abons_id', '=', 'user_abons.id')
                         ->where('user_abons.id','=',$user_Abonne)
                         ->select('*','demande_quotas.id as id','user_abons.id as user_abons_id')
                         ->orderByRaw('demande_quotas.id DESC')
                         ->get();
            return $List_demandeQuotas;
    }

}
