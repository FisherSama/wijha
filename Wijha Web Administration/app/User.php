<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                'nom','prenom','organisme','num_telephone', 'email', 'password','role','confirmed_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function groupes(){
        return $this->belongsToMany(Groupe::class);
    }

    public function layouts(){
        return $this->hasMany('App\Layout');
    }

    public function fonctionnalites(){
        return $this->belongsToMany('App\Fonctionnalite','user_foncts');
    }
    public function medias(){
        return $this->hasMany('App\Media');

    }

    public function abonnements(){
        return $this->belongsToMany('App\Abonnement','user_abons')->withPivot('id','quota_disponible', 'nbr_afficheur_dispo'
                                                                        ,'date_debut','date_fin');
    }

    public function my_groups()
    {
        return $this->hasMany('App\Groupe');
    }

    public function demande_quotas(){
        return $this->hasMany('App\Demande_quota');
    }

     public function myTasks(){
        return $this->hasMany('App\Task');
    }

     public function screens()
    {
        return $this->hasMany('App\Screen');
    }



}
