<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
class Languages extends Model
{
    //

     public static function checkLanguage() {

       if(null !==(session('lng'))) {
         App::setLocale(session('lng'));  
        } else {
         App::setLocale('en');   
        }
     
     }
}
