<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
    public function layout()
    {
    	return $this->belongsTo('App\Layout');
    }

    public function medias(){
        return $this->belongsToMany('App\Media', 'media_regions');
    }
}
