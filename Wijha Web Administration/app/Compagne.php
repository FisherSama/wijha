<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compagne extends Model
{
    //
    public function groupes()
    {
    	return $this->belongsToMany(Groupe::class);
    }
}
