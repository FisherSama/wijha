<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abonnement extends Model
{
    //


    public function users(){
        return $this->belongsToMany('App\User','user_abons')->withPivot('id','quota_disponible', 'nbr_afficheur_dispo','date_debut','date_fin');
    }


     public function groups(){
        return $this->belongsToMany('App\Groupe','group_abon');
     }
}
