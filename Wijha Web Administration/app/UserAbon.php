<?php

namespace App;
Use DB;
use Illuminate\Database\Eloquent\Model;

class UserAbon extends Model
{
    //

    public function demande_quotas(){
        return $this->hasMany('App\Demande_quota');
    }

    function getAll_Abonne($start,$limit){
            $List_abonnes = User::join('user_abons', 'users.id', '=', 'user_abons.user_id')
                        ->join('abonnements', 'user_abons.abonnement_id', '=', 'abonnements.id')
                         ->offset($start)
                         ->limit($limit)
                         ->select('*','abonnements.id as id_abonnements')
                         ->get();
            return $List_abonnes;                   
    }


    function getAllAbonne_filtred($search,$start,$limit)  {
    	$List_abonnes = User::join('user_abons', 'users.id', '=', 'user_abons.user_id')
                             ->join('abonnements', 'user_abons.abonnement_id', '=', 'abonnements.id')
                             ->where('users.nom','LIKE',"%{$search}%")
                             ->orWhere('users.prenom', 'LIKE',"%{$search}%")
                             ->orWhere('users.organisme', 'LIKE',"%{$search}%")
                             ->orWhere('users.num_telephone', 'LIKE',"%{$search}%")
                             ->orWhere('users.email', 'LIKE',"%{$search}%")
                             ->offset($start)
                             ->limit($limit)
                             ->select('*','abonnements.id as id_abonnements')
                             ->get();

        return $List_abonnes;                     
    }

    function getAllAbonne_filtred_withAbonnement($search,$start,$limit,$abonnement_choice)  {

        $List_abonnes = User::join('user_abons', 'users.id', '=', 'user_abons.user_id')
                             ->join('abonnements', 'user_abons.abonnement_id', '=', 'abonnements.id')
                             ->where('user_abons.abonnement_id','=',$abonnement_choice)
                              ->where(function($query) use ($search) {
                                 $query->where('users.nom','LIKE',"%{$search}%")
                                       ->orWhere('users.prenom', 'LIKE',"%{$search}%")
                                       ->orWhere('users.organisme', 'LIKE',"%{$search}%")
                                       ->orWhere('users.num_telephone', 'LIKE',"%{$search}%")
                                       ->orWhere('users.email', 'LIKE',"%{$search}%");
                                })
                             ->offset($start)
                             ->limit($limit)
                             ->select('*','abonnements.id as id_abonnements')
                             ->get();

        return $List_abonnes;                     
    }

        function construct_data($posts){
    	$data = array();
    	foreach ($posts as $post)
            {

                $nestedData['nom'] = $post->nom;
                $nestedData['organisme'] = $post->organisme;
                $nestedData['email'] = $post->email;
                $nestedData['num_telephone'] = $post->num_telephone ;
                $nestedData['date_debut'] = date('d/m/Y h:i a',strtotime($post->date_debut)) ;
                $nestedData['date_fin'] =   date('d/m/Y h:i a',strtotime($post->date_fin));

                $data[] = $nestedData;

            }
            return $data;
    }


    public function abonne_willEnd(){

        $List_abonnes = UserAbon::whereRaw('DATEDIFF(day,date_fin,date("Y-m-d")) <= ?')
            ->setBindings([7])
            ->get();

        return $List_abonnes;    
    }

    public static function abonne_valide(){
        
        $List_abonnes = DB::table('user_abons')->whereRaw('DATEDIFF(date_fin,date("Y-m-d")) >= 0')
            ->get();

        return $List_abonnes;    
    }


    public function abonne_expired(){

        $List_abonnes = UserAbon::whereRaw('DATEDIFF(day,date_fin,date("Y-m-d")) = ?')
            ->setBindings([0])
            ->get();

        return $List_abonnes;    
    }

}
