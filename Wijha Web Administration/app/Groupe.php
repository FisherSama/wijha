<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groupe extends Model
{
    //

    public function users(){
        return $this->belongsToMany('App\User', 'user_groupes');
    }

    public function compagnes(){
        return $this->belongsToMany('App\Compagne', 'compagne_groupes');
    }

    public function medias(){
        return $this->belongsToMany(Media::class);
    }

     public function my_admin()
    {
        return $this->belongsTo('App\User');
    }

     public function fonctionnalites(){
        return $this->belongsToMany('App\Fonctionnalite','user_foncts');
     }


      public function abonnements(){
        return $this->belongsToMany('App\Abonnement','group_abon');
     }
 

}
