<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class Languages extends Controller
{
    public function __construct()
    {
       
       // $this->middleware('auth');
    }

    public function index(Request $request,$lng){

    	$languages  = $lng;
    	$request->session()->put('lng',$lng);
    	$this-> change_language($lng);

        return back();
    	
    	
    }


    public function change_language($lng) {
         App::setLocale($lng);   
    }
}
