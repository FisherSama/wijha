<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Languages;
use App\Abonnement;
class AbonnementController extends Controller
{
    //
      public function __construct()
      {
      
        
        $this->middleware('auth');
      }

       public function index()
       {  
         Languages::checkLanguage();
         $list_abonnements = Abonnement::all();
         return View('abonnement.abonnement')->with('list_abonnements',$list_abonnements);
       }

       public function create(Request $request) {
           $abonnement = new Abonnement;
        
                $this->validate($request, [
                    
                          'licence' => 'required|numeric|min:1',
                          'tarification' => 'required|numeric|min:1000',
                          'quota' => 'required|max:2000',
                          'max_user' => 'required|numeric',
                          'duree' => 'required|numeric|min:1',
                      ]);
          
            $abonnement->titre_abonnement = $request->titre_abonnement;
            $abonnement->licence = $request->licence;
            $abonnement->tarification = $request->tarification ;
            $abonnement->quota = $request->quota ;
            $abonnement->max_user = $request->max_user ;
            $abonnement->duree = $request->duree ; 
            $abonnement->save();
            return back()->with('success','Abonnement Has been added successfully.'); 
       }


       public function edit(Request $request){
       	   $abonnement = Abonnement::findOrFail($request->abonnement_ID);
           
                $this->validate($request, [
                    
                          'licence' => 'required|numeric|min:1',
                          'tarification' => 'required|numeric|min:1000',
                          'quota' => 'required|max:2000',
                          'max_user' => 'required|numeric',
                          'duree' => 'required|numeric|min:1',
                      ]);
            
            $abonnement->titre_abonnement = $request->titre_abonnement;
            $abonnement->licence = $request->licence;
            $abonnement->tarification = $request->tarification ;
            $abonnement->quota = $request->quota ;
            $abonnement->max_user = $request->max_user ;
            $abonnement->duree = $request->duree ; 
            $abonnement->save();
            return back()->with('success','Abonnement Has been updated successfully.');
       }

       public function delete(Request $request){
       	    $abonnement = Abonnement::findOrFail($request->abonnement_ID);

            if($abonnement->users()->first() === null){
              $abonnement->delete();
              return back()->with('success','Abonnement Has been deleted successfully.');
            }
            else{
            	return back()->with('warning','you can not deleted, this entity is already used by other users');
            }

       }
}
