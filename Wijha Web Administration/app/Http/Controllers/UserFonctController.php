<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use App;
use App\Languages;
use App\Menu;
use App\Fonctionnalite;
use App\UserFonct;
use App\Groupe;

class UserFonctController extends Controller
{
    //

     public function __construct()
    {
        
        $this->middleware('auth');
    }

     public function index($id_groupe){

        Languages::checkLanguage();

        $menus=Menu::all();
        $all_menus=array();

    	foreach ($menus  as $menu) {

            $current_menu=array();
            $fonctionnalites=Menu::find($menu->id)->fonctionnalites->where('type','==','not_superUser');
            $current_menu['nom_menu']=$menu->nom_menu;
            $current_menu['list']=$fonctionnalites;
            array_push($all_menus,$current_menu);
            
    	}


        $permission_group=Groupe::find($id_groupe)->fonctionnalites;
        $list_permission=array();

        $nom_groupe=Groupe::find($id_groupe)->nom_groupe;


        foreach ($permission_group as $key) {
            array_push($list_permission, $key->nom_fonctionnalite);
        }


        return view('group_user.permission')->with(['all_menus'=>$all_menus,
                                                    'list_permission'=>$list_permission,
                                                    'id_groupe'=>$id_groupe,
                                                    'nom_groupe'=>$nom_groupe,
                                                    ]);

      

    }


     public function updatePermission(Request $request) {

       
       $id_groupe=$request->get('id_group');
       $list_choice=json_decode($request->get('list_choice'));
     
       $groupe= App\Groupe::find($id_groupe);

       
       foreach ($list_choice as $key) {
          $groupe->fonctionnalites()->detach($key->id);
        }

       foreach ($list_choice as $key) {

          if($key->etat=="true") {

           $grp_fct=new UserFonct;
           $grp_fct->groupe_id= $id_groupe;
           $grp_fct->fonctionnalite_id= $key->id;
           $grp_fct->save();
          
           }
          

          
       }


     }


}
