<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\UserAbon;
use App\Abonnement;
use App\User;
use App\Demande_quota;



use App;
use App\Languages;
use Auth;
class UserAbonController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {  
         Languages::checkLanguage();    
       
    }

    public function ourAbonne(){
        
        $list_abonnements = Abonnement::all();
        $total_abonne = UserAbon::count();
        
        $total_valide = UserAbon::abonne_valide();
        
    	return View('abonnement.mesAbonne')->with('list_abonnements',$list_abonnements)
                                           ->with('total_abonne',$total_abonne)
                                           ->with('abonne_valide',$total_valide)
                                           ->with('abonne_nonrenouveller',$total_abonne)
                                           ->with('abonne_expirer',$total_abonne);
    }


       public function allAbonne(Request $request)
    {   

        $userAbon = new UserAbon;
        $columns = array( 
                            0 =>'nom', 
                            1 =>'organisme',
                            2=> 'email',
                            3=> 'num_telephone',
                            4=> 'date_debut',
                            5=> 'date_fin',
                        );
  
        $totalData = UserAbon::count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')) && $request->id_abonnement == "0" )
        {            
          $posts = $userAbon->getAll_Abonne($start,$limit);
        }
        elseif($request->id_abonnement == "0") {

            $search = $request->input('search.value');       
            $id_abonnement_choisie = (int) $request->id_abonnement;
            $posts = $userAbon->getAllAbonne_filtred($search,$start,$limit);
            $totalFiltered = count($posts);
        }
        else{
    
            $search = $request->input('search.value');            
            $id_abonnement_choisie = (int) $request->id_abonnement;
            $posts = $userAbon->getAllAbonne_filtred_withAbonnement($search,$start,$limit,$id_abonnement_choisie);
            $totalFiltered = count($posts);

        }

        $data = array();

        if(!empty($posts))
        {
           $data = $userAbon->construct_data($posts);
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
        
    }



    public function abonneByAbonnement(Request $request){

        $userAbon = new UserAbon;

         $List_abonnes = Abonnement::findOrFail($request->id)->users;
         $data = array();

        if(!empty($List_abonnes))
        {
           $data = $userAbon->construct_data($List_abonnes);
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval(count($List_abonnes)),  
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);

    }

    public function myAbonnement(){

        $userAbon = new UserAbon;

        $id = Auth::id();

         $list_abonnements = User::findOrFail($id)->abonnements;
         return View('abonnement.monAbonnement')->with('list_abonnements',$list_abonnements);

        
    }
    

}
