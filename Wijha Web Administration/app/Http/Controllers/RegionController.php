<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Languages;


class RegionController extends Controller
{
    //

    public function __construct()
    {
    
        $this->middleware('auth');
    }


     public function index()
    {  
         Languages::checkLanguage();
         //return view('region.region');
        
    }

       public function edit()
    {  
         Languages::checkLanguage();
         return view('region.edit');
         
        
    }
}
