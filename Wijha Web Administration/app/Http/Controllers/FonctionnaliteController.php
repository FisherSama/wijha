<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fonctionnalite;

use App;
use App\Languages;

class FonctionnaliteController extends Controller
{
    //

      public function __construct()
    {
       

        if(null !==(session('lng'))) {
         App::setLocale(session('lng'));  
        
        } else {
         App::setLocale('en');   
        }
        
        $this->middleware('auth');
    }

    public function index()

    {  
         Languages::checkLanguage();
         //return view('dashboard');
    }
}
