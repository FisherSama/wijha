<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Languages;
use App\Demande_quota;
use App\UserAbon;
use Auth;

class DemandeQuotaController extends Controller
{
    //
      public function __construct()
    {  
        $this->middleware('auth');
    }

    public function index()

    {  
         Languages::checkLanguage();
         return View('abonnement.demandeQuota');
    }
    

    public function alldemande(Request $request){
       $demande_quota = new Demande_quota;
       
        $columns = array( 
                            0 =>'code_demande', 
                            1 =>'nom',
                            2=> 'organisme',
                            3=> 'num_telephone',
                            4=> 'etat',
                            5=> 'date',
                            6=> 'option',
                        );
  
        $totalData = Demande_quota::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
          $posts = $demande_quota->getAll_demandeQuotas($start,$limit);
        }
        else {
            $search = $request->input('search.value'); 
            $posts = $demande_quota->getAll_demandeQuotas_filtred($search,$start,$limit);
            $totalFiltered = count($posts);
        }

        $data = array();

        if(!empty($posts))
        {
           $data = $demande_quota->construct_data($posts);
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
    }


    public function newDemande(Request $request){

        $demande_quota = new Demande_quota;

        $this->validate($request, [
                    
                          'userAbonne' => 'required|numeric|min:1',
                          'licence' => 'required|numeric',
                          'quota' => 'required|numeric|max:2000',
                          'max_user' => 'required|numeric',
                          'titre' => 'required|string|max:80',
                      ]);

        $userAbonne = UserAbon::findOrFail($request->userAbonne);

        $demande_quota->user_abons_id = $userAbonne->id;
        $demande_quota->user_id = Auth::id();

        $demande_quota->nbr_licence = $request->licence;
        $demande_quota->espace_demander = $request->quota;  
        $demande_quota->nbr_user = $request->max_user;
        $demande_quota->etat_quota = 'En attente';
        $demande_quota->titre_demande = $request->titre;
        $demande_quota->numero_demande = "0321156488994";

        $demande_quota->save();

        return back()->with('success','Demande Has been sent successfully.');
        
    }


    public function validation(Request $request) {
       $demande_quota = Demande_quota::findOrFail($request->id);
       
       // récupérer l'abonnement afin de pouvoir modifer les quota disponible selon la demande formule
       $userAbon = UserAbon::findOrFail($demande_quota->user_abons_id);

       $userAbon->nbr_afficheur_dispo = $userAbon->nbr_afficheur_dispo + $demande_quota->nbr_afficheur ;
       $userAbon->nbr_licence_dispo = $userAbon->nbr_licence_dispo + $demande_quota->nbr_licence ;

       $userAbon->nbr_user_disponible = $userAbon->nbr_user_disponible + $demande_quota->nbr_user ;
       $userAbon->quota_disponible = $userAbon->quota_disponible + $demande_quota->espace_demander ; 
       $userAbon->save();

       //modfier état de la demande une la modification des quotas disponible effectuer

        $demande_quota->etat_quota = 'validé';
        $demande_quota->save();
        $response = array(
            'status' => 'success',
            'msg' => 'demande validé avec succés',
        );
        return \Response::json($response);
    }

    
    public function refuser(Request $request) {
        $demande_quota = Demande_quota::findOrFail($request->id);
        $demande_quota->etat_quota = 'refuser';
        $demande_quota->save();
        $response = array(
            'status' => 'success',
            'msg' => 'demande décliner avec succés',
        );
        return \Response::json($response);
    }
    


}
