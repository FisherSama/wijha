<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Languages;

class TemplateLayoutController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {  
         Languages::checkLanguage();
        
    }
}
