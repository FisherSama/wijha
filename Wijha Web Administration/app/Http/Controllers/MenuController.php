<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

use App;
use App\Languages;

class MenuController extends Controller
{
    //

    public function __construct()
    {
       

        if(null !==(session('lng'))) {
         App::setLocale(session('lng'));  
        
        } else {
         App::setLocale('en');   
        }
        
        $this->middleware('auth');
    }

    public function index()
    {  
         Languages::checkLanguage();

        /* $fct=App\Menu::find(2)->fonctionnalites;
         dd($fct);*/
       
    }
}
