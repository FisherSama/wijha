<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Groupe;
use App\UserAbon;

use App;
use DB;
use App\Languages;
use Illuminate\Support\Facades\Hash;

class GroupeController extends Controller
{
    //
    public function __construct()

    {       
        $this->middleware('auth');
    }

    function index(){

         Languages::checkLanguage();
         $my_groups=App\User::findOrFail(auth()->user()->id)->my_groups;

         $userAbon = new UserAbon;
         $list_abonnements = User::findOrFail(auth()->user()->id)->abonnements;
         //dd($list_abonnements);

         return view('group_user.my_groups')->with('my_groups',$my_groups);

    }


    function addGroup(Request $request){


         $groupe=new Groupe;
         $groupe->nom_groupe=$request->get('nom_groupe');
         $groupe->user_id=auth()->user()->id;
         $groupe->save();


         dd($groupe->id);

         /*return back()->with('success','Group Has been added successfully.');*/
	
    }

     function deleteGroup(Request $request){

         $groupe=Groupe::findOrFail($request->get('id_groupe'));
         $nbr_user=count(App\Groupe::findOrFail($request->get('id_groupe'))->users);
         
         if($nbr_user>0) {
           return back()->with('warning','Group conatin members can\'t  be deleted .');
         }else {

          $groupe->delete();
          return back()->with('success','Group Has been deleted successfully.'); 

         }

         /*$groupe->delete();
         return back()->with('success','Group Has been deleted successfully.');*/
	
    }

      function updateGroup(Request $request){

         $groupe=Groupe::findOrFail($request->get('id_groupe'));
         $groupe->nom_groupe=$request->get('nom_groupe');
         $groupe->save();

         return back()->with('success','Group Has been updated successfully.');
	
    }


    function membersGroupe($id_groupe){
        Languages::checkLanguage();
        $list_members=App\Groupe::findOrFail($id_groupe)->users;
        return view('group_user.members_groupe')->with('list_members',$list_members);

    }


    function membersAllgroupsOfAdmin(){

      Languages::checkLanguage();
      $my_groups=App\User::findOrFail(auth()->user()->id)->my_groups;

      $users_list = DB::table('users')
            ->join('user_groupes', 'users.id', '=', 'user_groupes.user_id')
            ->join('groupes', 'user_groupes.groupe_id', '=', 'groupes.id')
            ->select('users.*', 'groupes.nom_groupe')
            ->where('groupes.user_id', '=',auth()->user()->id)
            ->get();
 
        return view('group_user.users_list')->with([
        'my_groups'=> $my_groups,
        'users_list'=> $users_list,
        ]); 


    }

}
