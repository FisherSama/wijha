<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Languages;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\User_groupe;
use Auth;
use App\Task;

class UserController extends Controller
{
    //

    public function __construct()
    {     
       $this->middleware('auth');
    }

    public function index()
    {  
         Languages::checkLanguage();

         $user_role = auth()->user()->role;
         
         if($user_role=="Admin"){
            return view('region.region');
         }
         elseif($user_role=="super_admin"){
         // return view('dashboard_admin');
            return view('region.region');
         }
         else{
            return view('region.region');
         }

    }


    public function addSimpleUser(Request $request){

         $user = new User;

         $user->nom=$request->get('nom');
         $user->prenom=$request->get('prenom');
         $user->organisme=$request->get('organisme');
         $user->num_telephone=$request->get('num_telephone');
         $user->email=$request->get('email');
         $user->password=Hash::make($request->get('password'));
         $user->etat="oui";
         $user->role="simple_user";
         $user->confirmed_code="0000";

         $id_groupe=$request->get('groupe');
         $user->save();

         $user_group=new User_groupe;

         $user_group->user_id=$user->id;;
         $user_group->groupe_id=$id_groupe;

         $user_group->save();

       

        return back()->with('success','User Has been created successfully.');

    }



    public function deleteUser(Request $request) {

      $id_user=$request->get('id_user');
      $user=App\User::findOrFail($id_user);
      $user->delete();
      return back()->with('success','User Has been deleted successfully.');
      
    }


    public function profil(){

     Languages::checkLanguage();

     $user=App\User::findOrFail(auth()->user()->id);
     $user_tasks=App\User::findOrFail(auth()->user()->id)->myTasks;

     return view('group_user.profil')->with([
                                             'user'=>$user,
                                             'user_tasks'=>$user_tasks,
                                              ]);

    }


    public function updateUser(Request $request) {

      $id_user=$request->get('id_user');
      $user=App\User::findOrFail($id_user);
    
      $user->nom=$request->get('nom');
      $user->prenom=$request->get('prenom');
      $user->email=$request->get('email');
      $user->num_telephone=$request->get('num_telephone');
      $user->organisme=$request->get('organisme');

      if(Hash::check($request->get('old_password'),$user->password)) {

        $user->fill([
            'password' => Hash::make($request->get('new_password'))
        ])->save();

        return back()->with('success','User data Has been updated successfully.');

      }  else {

        return back()->with('warning','Verify your password.');
      }

    
     
    }


    public function addTask(Request $request) {

        $task=new Task;

        $task->name_task=$request->get('task');
        $task->user_id=auth()->user()->id;
        $task->save();
        

    
        
    }


    public function updateTask(Request $request){

        $task=App\Task::findOrFail($request->get('id'));
        $task->etat=$request->get('etat');
        $task->save();

       
    }

   
}
