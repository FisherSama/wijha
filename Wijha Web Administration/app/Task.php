<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //

    public function my_user()
    {
        return $this->belongsTo('App\User');
    }
}
