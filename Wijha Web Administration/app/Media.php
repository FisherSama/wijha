<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function groupes(){
        return $this->belongsToMany('App\Groupe', 'media_groupes');
    }

    public function regions(){
        return $this->belongsToMany(Region::class);
    }
    
}
