<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fonctionnalite extends Model
{
    //
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

    public function users(){
        return $this->belongsToMany('App\User','user_foncts');
    }

    public function groupes(){
        return $this->belongsToMany('App\User','user_foncts');
    }
}
