<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //

    public function fonctionnalites()
    {
        return $this->hasMany('App\Fonctionnalite');
    }
}
