<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    //
    public function layout()
    {
        return $this->hasOne('App\Layout');
    }

}
