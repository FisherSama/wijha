<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    //
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    
    public function template()
    {
    	return $this->belongsTo('App\Template');
    }

        public function regions(){
        return $this->hasMany('App\Region');
    }
    
}
