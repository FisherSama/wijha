<?php 



return [
    'welcome' => 'Welcome to our application',

    'Menu_dictionnaire' => '---------------------------------------',

    'dashboard' => 'Dashboard',
    'planning' => 'Planning',
    'layout' => 'Layout',
    'display' => 'Display',
    'library' => 'Library',
    'administrator' => 'Administrator',
    'logs' => 'Logs',
    'statistics' => 'Statistics',
    'log_out' => 'Log out',
    'email' => 'Email',
    'password' => 'Password',
    'users' => 'Users',

    'language' => 'language',
    'users_group' => 'Users group',
    'users_list' => 'Users list',
    'new_group' => 'New Group',

    'name_group' => 'Group Name',
    'date_creation' => 'Date Creation',
    'operations' => 'Operations',
    'consulte' => 'Consult',
    'add' => 'Add',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'update' => 'Update',
    'confirmation_delete' => 'Confirm Delete',


    'first_name' => 'First Name',
    'family_name' => 'Family Name',
    'organization' => 'Organization',
    'phone' => 'Phone',

    'abonnement' => 'Subscription',
    'nos_abonnements' => 'Our subscriptions',
    'nos_abonnes' => 'Our subscribers',
    'requete_abonnes' => 'Request subscribers',
    'mon_abonnement' => 'My subscription',

    'permission' => 'Permission to access',

    'work' => 'Work at',
    'inscription_date' => 'Date registered',

];

