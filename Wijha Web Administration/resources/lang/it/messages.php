<?php 



return [
    'welcome' => 'Benvenuti nella nostra applicazione',


    'Menu_dictionnaire' => '---------------------------------------',

    'dashboard' => 'Cruscotto',
    'planning' => 'Pianificazione',
    'layout' => 'Disposizione',
    'display' => 'Display',
    'library' => 'Biblioteca',
    'administrator' => 'Amministratore',
    'logs' => 'Logs',
    'statistics' => 'Statistica',
    'log_out' => 'disconnettersi',
    'email' => 'e-mail',
    'password' => 'parola d\'ordine',
    'users' => 'le utenza',


    'language' => 'la lingua',
    'users_group' => 'gruppo di utenti',
    'users_list' => 'Elenco degli utenti',
    'new_group' => 'nuovo gruppo',

    'name_group' => 'Nome del gruppo',
    'date_creation' => 'Data di creazione',
    'operations' => 'Operazioni',
    'consulte' => 'Consultare',
    'add' => 'Inserisci',
    'delete' => 'Elimina',
    'edit' => 'Edit',
    'update' => 'Modificare',
    'confirmation_delete' => 'Conferma cancellazione',

    'first_name' => 'Nome di battesimo',
    'family_name' => 'Cognome',
    'organization' => 'Organizzazione',
    'phone' => 'Telefono',

    'abonnement' => 'Abbonamento',
    'nos_abonnements' => 'I nostri abbonamenti',
    'nos_abonnes' => 'I nostri abbonati',
    'requete_abonnes' => 'Richiedi abbonati',
    'mon_abonnement' => 'Il mio abbonamento',

    'permission' => 'Permesso di accesso',

    'work' => 'Lavorare a',
    'inscription_date' => 'Data di registrazione',
];

