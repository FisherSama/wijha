<?php 



return [
    'welcome' => 'Bienvenido a nuestra aplicación',


    'Menu_dictionnaire' => '---------------------------------------',

    'dashboard' => 'tablero',
    'planning' => 'Planificación',
    'layout' => 'Diseño',
    'display' => 'Monitor',
    'library' => 'Biblioteca',
    'administrator' => 'Administrador',
    'logs' => 'Logs',
    'statistics' => 'Estadística',
    'log_out' => 'Cerrar sesión',
    'email' => 'correo electrónico',
    'password' => 'contraseña',
    'users' => 'Usuarios',


    'language' => 'la lengua',
    'users_group' => 'grupo de usuarios',
    'users_list' => 'Lista de usuarios',
    'new_group' => 'Nuevo grupo',

    'name_group' => 'Nombre del grupo',
    'date_creation' => 'Fecha de creación',
    'operations' => 'Operaciones',
    'consulte' => 'Consultar',
    'add' => 'Añadir',
    'delete' => 'Borrar',
    'edit' => 'Editar',
    'update' => 'Actualizar',
    'confirmation_delete' => 'Confirmar eliminación',


    'first_name' => 'Nombre de pila',
    'family_name' => 'Apellido',
    'organization' => 'Organización',
    'phone' => 'Teléfono',

    'abonnement' => 'Suscripción',
    'nos_abonnements' => 'Nuestras suscripciones',
    'nos_abonnes' => 'Nuestros suscriptores',
    'requete_abonnes' => 'Solicitar suscriptores',
    'mon_abonnement' => 'Mi suscripción',

    'permission' => 'Permiso de acceso',

    'work' => 'Trabajar en',
    'inscription_date' => 'Fecha de registro,
];

