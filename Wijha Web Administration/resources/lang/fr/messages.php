<?php 



return [
    'welcome' => 'Bienvenue à notre application',

    'Menu_dictionnaire' => '---------------------------------------',

    'dashboard' => 'Tableau de bord',
    'planning' => 'Planification',
    'layout' => 'Mise en page',
    'display' => 'Afficheur',
    'library' => 'Bibliothèque',
    'administrator' => 'Administrateur',
    'logs' => 'Logs',
    'statistics' => 'Statistiques',
    'log_out' => 'déconnexion',
    'email' => 'email',
    'password' => 'mot de passe',
    'users' => 'Utilisateurs',

    'language' => 'langue',
    'users_group' => 'groupe d\'utilisateurs',
    'users_list' => 'Liste d\'utilisateurs',
    'new_group' => 'Nouveau Groupe',

    'name_group' => ' Nom de groupe',
    'date_creation' => 'Date Creation',
    'operations' => 'Operations',
    'consulte' => 'Consulter',
    'add' => 'Ajouter',
    'delete' => 'Supprimer',
    'edit' => 'Editer',
    'update' => 'Mettre à jour',
    'confirmation_delete' => 'Confirmer la supression',

    'first_name' => 'Prénom',
    'family_name' => 'Nom',
    'organization' => 'Organisation',
    'phone' => 'téléphone',

    'abonnement' => 'Abonnement',
    'nos_abonnements' => 'Nos abonnements',
    'nos_abonnes' => 'Nos abonnés',
    'requete_abonnes' => 'Requete Abonnés',
    'mon_abonnement' => 'Mon abonnement',

    'permission' => 'Droit d\'accés',

    'work' => 'Travaille à',
    'inscription_date' => 'Date Inscription',

];

