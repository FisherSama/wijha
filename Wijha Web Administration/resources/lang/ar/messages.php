<?php 



return [
    'welcome' => 'مرحبا بكم ',

    'Menu_dictionnaire' => '---------------------------------------',

    'dashboard' => 'لوحة التحكم',
    'planning' => 'الجدولة',
    'layout' => 'التصميم',
    'display' => 'العرض',
    'library' => 'المكتبة',
    'administrator' => 'ادارة التحكم',
    'logs' => 'الرساءل',
    'statistics' => 'الاحصاىيات',
    'log_out' => 'الخروج',
    'email' => 'البريد الإلكتروني',
    'password' => 'كلمه السر',
    'users' => 'المستخدمين',


    'language' => 'اللغة',
    'users_group' => 'مجموعات المستخدمين',
    'users_list' => 'قائمة المستخدمين',
    'new_group' => 'مجموعة جديدة',

    'name_group' => 'اسم المجموعة',
    'date_creation' => 'تاريخ الإنشاء',
    'operations' => 'العمليات',
    'consulte' => 'المعاينة',
    'add' => 'أضف',
    'delete' => 'حذف',
    'edit' => 'تعديل',
    'update' => 'تحديث',
    'confirmation_delete' => 'تأكيد الحذف',

    'first_name' => 'الإسم ',
    'family_name' => 'اللقب',
    'organization' => 'الشركة',
    'phone' => 'الهاتف',

    'abonnement' => 'الاشتراك',
    'nos_abonnements' => 'اشتراكاتنا',
    'nos_abonnes' => ' مشتركينا',
    'requete_abonnes' => 'طلبات الاشتراك',
    'mon_abonnement' => 'اشتراكاتي',

    'permission' => 'تحديد الصلاحيات',

    'work' => 'يعمل في ',
    'inscription_date' => 'تاريخ التسجيل',
];

