<?php 



return [
    'welcome' => 'Bem-vindo ao nosso aplicativo',

     'Menu_dictionnaire' => '---------------------------------------',

    'dashboard' => 'Painel de controle',
    'planning' => 'Planejamento',
    'layout' => 'Layout',
    'display' => 'Exibição',
    'library' => 'Biblioteca',
    'administrator' => 'Administrador',
    'logs' => 'Logs',
    'statistics' => 'Estatisticas',
    'log_out' => 'sair',
    'email' => 'o email',
    'password' => 'senha',
    'username'=>'username',
    'users' => 'Do utilizador',


    'language' => 'língua',
    'users_group' => 'grupo de usuários',
    'users_list' => 'Lista de usuários',
    'new_group' => 'novo grupo',

    'name_group' => 'Nome do grupo',
    'date_creation' => 'Data de criação',
    'operations' => 'Operações',
    'consulte' => 'Consultar',
    'add' => 'Adicionar',
    'delete' => 'Excluir',
    'edit' => 'Editar',
    'update' => 'Atualizar',
    'confirmation_delete' => 'Confirme a exclusão',

    'first_name' => 'Primeiro nome',
    'family_name' => 'Sobrenome',
    'organization' => 'Organização',
    'phone' => 'Telefone',

    'abonnement' => 'Subscrição',
    'nos_abonnements' => 'Nossas inscrições',
    'nos_abonnes' => 'Nossos assinantes',
    'requete_abonnes' => 'Solicitar inscritos',
    'mon_abonnement' => 'Minha assinatura',

    'permission' => 'Permissão para acessar',

    'work' => 'Trabalhar em',
    'inscription_date' => 'Data registrada',
];

