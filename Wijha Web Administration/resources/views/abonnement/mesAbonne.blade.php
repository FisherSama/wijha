    @extends('layouts.layout_admin')

    @section('css')
        <link rel="stylesheet" href="{{ asset('js/datatables/datatables.css') }}">
    @endsection

    @section('content')
    <div class="row">

      <div class="col-sm-4">        
        <div class="tile-stats tile-primary" style="box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
          <div class="icon"><i class="entypo-suitcase"></i></div>
          <div class="num" data-start="0" data-end="{{ $total_abonne }}"  data-postfix=" " data-duration="1500" data-delay="0">0 </div>
          
          <h3>Total Abonnés</h3>
          <p>Les utilisateur ayant un abonnement valide ou l'ont été au moins une fois </p>
        </div>        
      </div>

      <div class="col-sm-4">      
        <div class="tile-stats tile-cyan" style="box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
          <div class="icon"><i class="entypo-check"></i></div>
          <div class="num" data-start="0" data-end="{{ $abonne_valide }}"  data-duration="1500" data-delay="0">0 &pound;</div>
          
          <h3>En cours de validé</h3>
          <p>Les utilisateur ayant un abonnement actif et en cours de validité</p>
        </div>      
      </div>

      <div class="col-sm-4">        
        <div class="tile-stats tile-red" style="box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
          <div class="icon"><i class="entypo-suitcase"></i></div>
          <div class="num" data-start="0" data-end="{{ $total_abonne+20 }}" data-duration="1500" data-delay="0">0 &pound;</div>
          
          <h3>Abonnement Expiré</h3>
          <p>Les utilisateurs ayant un abonnement déjà exprié et non renouveller</p>
        </div>  
      </div>
      
    </div>

    <div class="panel panel-default">
  <div class="panel-body">
          <div class="col-sm-5">
        <form role="form" class="form-horizontal form-groups-bordered"" action="/action_page.php">
    <div class="form-group">
                                <label class="control-label">Choisir un Abonnement :</label>
                                                                    
                                    <select name="test" class="form-control" id="Abonn_choisie">
                                        <option value="0"></option>
                                        @foreach($list_abonnements as $key => $abonnement)

                                        <option value="{{ $abonnement->id }}">{{ $abonnement->titre_abonnement }}</option>

                                        @endforeach
                                    </select>
                                    
                            </div>
</form>

    </div>
    <div class="col-sm-5"></div>
    <div class="col-sm-2" style="margin-left: 0px;">
        <button type="button" class="btn btn-primary btn-icon icon-left" data-toggle="modal" data-target="#createModal"><i class="entypo-plus"></i>  Nouveau abonné</button>
    </div>
  </div>
</div>


    <div class="row">

    	<div class="col-sm-12">
    		<div class="table-responsive">

            <table id="abonne" class="table table-bordered datatable">
              <thead>
               <tr>
                 <th>Nom</th>
                 <th>organisme</th>
                 <th>email</th>
                 <th>num_telephone</th>
                 <th>date_debut</th>
                 <th>date_fin</th>
                 <th>option</th>
               </tr>
             </thead>

          </table>
        </div>	

    	</div>

    </div>

<!--  ***********    Modal Create new Abonnée   **************** -->



  <div id="createModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="entypo-plus"></i>  {{ __('messages.add') }}</h4>
            </div>

            <div class="modal-body">
            <form class="form-horizontal" action="/user/add" method="post">
                    {{ csrf_field() }}


                    <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{{ __('messages.first_name') }}</label>
                        <div class="col-sm-8">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="entypo-user"></i></span>
              <input type="text" name="nom" class="form-control typeahead" required="true" />
                          </div>
                        </div>
                    </div>

            <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.family_name') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-user"></i></span>
              <input type="text" name="prenom" class="form-control typeahead" required="true" />
              </div>
              </div>
            </div>


            <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.organization') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-home"></i></span>
              <input type="text" name="organisme" class="form-control typeahead" required="true" />
              </div>
              </div>
            </div>


            <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.name_group') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-users"></i></span>
                 <select class="form-control typeahead" name="groupe" required="true">

                     <option value="">  </option>
                      @foreach($list_abonnements as $key => $abonnement)

                       <option value="{{ $abonnement->id }}">{{ $abonnement->titre_abonnement }}</option>

                      @endforeach  

                 </select>
              </div>
              </div>
            </div>


              <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.phone') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-phone"></i></span>
              <input type="text" name="num_telephone" class="form-control typeahead" required="true" />
              </div>
              </div>
            </div>

              <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.email') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-mail"></i></span>
              <input type="text" name="email" class="form-control typeahead" required="true" />
              </div>
              </div>
            </div>


           <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.password') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-key"></i></span>
              <input type="password" name="password" class="form-control typeahead" required="true" />
              </div>
              </div>
            </div>


            <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.password') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-key"></i></span>
              <input type="password" name="password2" class="form-control typeahead" required="true" />
              </div>
              </div>
            </div>

           




                    <div class="modal-footer">

                      <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#createModal">{{ __('messages.add') }}
                      </button>
                    </div>

             </form>
            </div>

            
        </div>


    </div>
  </div>

<!--  ***********    END  Modal Create new Abonnée   **************** -->

    @endsection('content')

@section('js')
<script src="{{ URL::to('js/datatables/datatables.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () {

  drawDataTable();


    function drawDataTable(){
       var id = $('#Abonn_choisie').val();

      $('#abonne').DataTable({
            "processing": true,
            "serverSide": true,
             "bDestroy": true,
            "ajax":{
                     "url": "{{ url('allAbonne') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}", id_abonnement:id}
                   },
            "columns": [
                { "data": "nom" },
                { "data": "organisme" },
                { "data": "email" },
                { "data": "num_telephone" },
                { "data": "date_debut" },
                { "data": "date_fin" }
            ]  

        })

    }


        $( "#Abonn_choisie" ).change(function() {
                
                   drawDataTable();

        });

    });
  
</script>

@endsection