    @extends('layouts.layout_admin')

    @section('css')
    <link rel="stylesheet" href="{{ asset('js/datatables/datatables.css') }}">
    @endsection

    @section('content')

    <div class="row">

    	<div class="col-sm-12">
    		<div class="table-responsive">

          <table id="abonne" class="table table-bordered datatable">
            <thead>
             <tr>
             </tr>
           </thead>
           <tbody>

            @foreach($list_abonnements as $key => $abonnement)

            <tr>
             <td>   <!-- Single Member -->
              <div class="member-entry">

                <a href="extra-timeline.html" class="member-img">
                  <img src="{{ asset('images/subscription.jpg') }}" class="img-rounded" />
                  <i class="entypo-forward"></i>
                </a>

                <div class="member-details">
                  <h4>
                  <a href="">{{ $abonnement->titre_abonnement }}</a>
                  </h4>

                  <!-- Details with Icons -->
                  <div class="row info-list">

                    <div class="col-sm-4">
                      <i class="entypo-calendar"></i>
                       de {{ $abonnement->pivot->date_debut }} à {{ $abonnement->pivot->date_fin }}
                    </div>

                    <div class="col-sm-4">
                      <i class="entypo-monitor"></i>
                      <a href="#">Afficheur restant : {{ $abonnement->pivot->nbr_afficheur_dispo }}</a>
                    </div>

                    <div class="col-sm-4">
                      <i class="entypo-facebook"></i>
                      Quota disponible : {{ $abonnement->pivot->quota_disponible }} GB
                    </div>

                    <div class="clear"></div>

                    <div class="col-sm-4">
                      <i class="entypo-users"></i>
                      <a href="#">Prishtina</a>
                    </div>

                    <div class="col-sm-4">
                      <i class="entypo-mail"></i>
                      <a href="#">Licence disponible : {{ $abonnement->pivot->nbr_licence_dispo }}</a>
                    </div>

                    <div class="col-sm-4">
                      <button type="button" class="btn btn-primary btn-icon icon-left" 
                      onclick="initForm({{ $abonnement->pivot->id }})" data-toggle="modal" data-target="#createModal">
                      <i class="entypo-plus"></i>  Nouveau Quota</button>
                    </div>

                  </div>
                </div>
                <br><br><br><br><br><br><br>

                <table class="table table-bordered datatable">
                  <thead>
                  <tr>
                  <th>Numéro</th>
                  <th>Date</th>
                  <th>Utilisateur</th>
                  <th>Licence</th>
                  <th>espace_demander</th>
                  <th>Etat</th>
                  <th>options</th>  
                  </tr></thead>
                  <tbody>
                    <?php $demandesBy_userAbon = App\Demande_quota::getDemandeuQuotas_ByUserAbonne($abonnement->pivot->id); ?>
                    @foreach($demandesBy_userAbon as $key => $demande)  
                    <tr>
                     <td>{{ $demande->id }}</td>
                     <td>{{ $demande->created_at }}</td>
                     <td>{{ $demande->nbr_user }}</td>
                     <td>{{ $demande->nbr_licence }}</td>
                     <td>{{ $demande->espace_demander }} GB</td>
                     <td>
                       @if ($demande->etat_quota =="validé") 
                       <span class="badge badge-success">validé</span>


                       @elseif($demande->etat_quota =="En attente")
                       <span class="badge badge-warning">En attente</span>
                       @else
                       <span class="badge badge-danger">Refusé</span>
                       @endif
                     </td>
                     <td><a><i class="entypo-print" style="colo

                     r: blue;"></i></a></td>
                   </tr>
                   @endforeach
                 </tbody>
                </table>

              </div>
              </td>

            </tr>
            @endforeach

          </tbody>

        </table>
      </div>	

    </div>

  </div>

    <!--********************************************* Modal nouveau abonnement ******************************************* -->
    <div id="createModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><i class="entypo-plus"></i>  Nouveau abonnement</h4>
          </div>
          <div class="modal-body">
          <form class="form-horizontal" action="{{ route('newDemande') }}"  method="post">
              {{ csrf_field() }}

              <div class="col-sm-8" hidden="true">
                    <input type="number" id="userAbonne" name="userAbonne" required="true" />
                </div>


              <div class="form-group">

                <label class="control-label col-sm-2" for="email">Titre de la demande</label>
                <div class="col-sm-8">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-monitor"></i></span>
                    <input type="text" name="titre" class="form-control typeahead" placeholder="mettez un titre pour votre commande" required="true" />
                  </div>
                </div>
              </div>  

              <div class="form-group">

                <label class="control-label col-sm-2" for="email">licence</label>
                <div class="col-sm-8">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-monitor"></i></span>
                    <input type="number" name="licence" class="form-control typeahead" value="0" required="true" />
                  </div>
                </div>
              </div>

              <div class="form-group">

                <label class="control-label col-sm-2" for="email">Quota</label>
                <div class="col-sm-8">
                  <div class="input-group">
                    <input type="number" name="quota" class="form-control typeahead" value="0" required="true" />
                    <span class="input-group-addon">GB</span>
                  </div>
                </div>
              </div>

              <div class="form-group">

                <label class="control-label col-sm-2" for="email">Max utilisateurs</label>
                <div class="col-sm-8">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-users"></i></span>
                    <input type="number" name="max_user" class="form-control typeahead" value="0" required="true" />
                  </div>
                </div>
              </div>
          
          </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-blue"><i class="entypo-check"></i>Ajouter</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
          </div>
          </form>
        </div>

      </div>
    </div>

  @endsection('content')

  @section('js')
  
  <script type="text/javascript">
    function initForm(id){
      $('#userAbonne').val(id);
    }
  </script>

  @endsection