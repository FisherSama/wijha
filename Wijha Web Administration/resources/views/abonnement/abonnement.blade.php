    @extends('layouts.layout_admin')
    @section('content')

    <div class="col-sm-10"></div>
    <div class="col-sm-2" style="margin-left: -15px;">
    	<button type="button" class="btn btn-primary btn-icon icon-left" data-toggle="modal" data-target="#createModal"><i class="entypo-plus"></i>  Nouveau abonnement</button>
    </div>
    <br><br>


    <div class="row">

    	<div class="col-sm-12">
    		<table class="table table-bordered datatable" id="table-1">
    			<thead>
    				<tr>
                        <th data-hide="phone">Titre</th>
    					<th data-hide="phone">Licence</th>
    					<th>Tarification</th>
    					<th data-hide="phone">Quota</th>
    					<th data-hide="phone,tablet">Nombre Utilisateur</th>
    					<th>Durée</th>
    					<th>Date Création</th>
    					<th>Opérations</th>
    				</tr>
    			</thead>
    			<tbody>
    			@foreach($list_abonnements as $key => $abonnement)
    				<tr>
                        <td>{{ $abonnement->titre_abonnement }}</td>
    					<td>{{ $abonnement->licence }}</td>
    					<td>{{ $abonnement->tarification }}</td>
    					<td>{{ $abonnement->quota }}</td>
    					<td class="center">{{ $abonnement->max_user }}</td>
    					<td class="center">{{ $abonnement->duree }}</td>
    					<td class="center">{{ $abonnement->created_at }}</td>
    					<td class="center">

                         <a role="button" class="btn btn-success btn-sm btn-icon icon-left" id="edit_abonnement" 
                         data-toggle="modal" data-target="#EditModal" data-titre="{{ $abonnement->titre_abonnement }}"
                          data-id="{{ $abonnement->id }}" data-licence="{{ $abonnement->licence }}"
                          data-tarification="{{ $abonnement->tarification }}" data-quota="{{ $abonnement->quota }}"
                          data-max_user="{{ $abonnement->max_user }}" data-duree="{{ $abonnement->duree }}"><i class="entypo-pencil"> </i>Modifier </a>


                        <a role="button" class="btn btn-danger btn-sm btn-icon icon-left" id="deleteAbonnement" data-toggle="modal" data-target="#deleteModal"
                         data-abonnement_ID="{{ $abonnement->id }}"><i class="entypo-trash"></i>Supprimé </a>

                         </td>

    				</tr>
                @endforeach
    			</tbody>
    			<tfoot>
    				<tr>
    					<th>Titre</th>
                        <th>Licence</th>
                        <th>Tarification</th>
                        <th>Quota</th>
                        <th>Nombre Utilisateur</th>
                        <th>Durée</th>
                        <th>Date Création</th>
                        <th>Opérations</th>
    				</tr>
    			</tfoot>
    		</table>	

    	</div>

    </div>

    <!--********************************************* Modal nouveau abonnement ******************************************* -->
    <div id="createModal" class="modal fade" role="dialog">
    	<div class="modal-dialog">

    		<!-- Modal content-->
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal">&times;</button>
    				<h4 class="modal-title"><i class="entypo-plus"></i>  Nouveau abonnement</h4>
    			</div>
    			<div class="modal-body">
    			<form class="form-horizontal" action="{{ route('newAbonnement') }}"  method="post">
    					{{ csrf_field() }}
                        <div class="form-group">

                            <label class="control-label col-sm-2" for="email">Titre</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                        <span class="input-group-addon"></span>
                                        <input type="text" name="titre_abonnement" class="form-control typeahead" placeholder="donnez un titre a l'abonnement" required="true" />
                                    </div>
                            </div>
                        </div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">licence</label>
    						<div class="col-sm-8">
    							<div class="input-group">
										<span class="input-group-addon"><i class="entypo-monitor"></i></span>
										<input type="number" name="licence" class="form-control typeahead" placeholder="nombre d'écran autorisé" required="true" />
									</div>
    						</div>
    					</div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">Tarification</label>
    						<div class="col-sm-8">
    							<div class="input-group">
										<input type="number" name="tarification" class="form-control typeahead"  required="true" />
										<span class="input-group-addon">DA</span>
									</div>
    						</div>
    					</div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">Quota</label>
    						<div class="col-sm-8">
    							<div class="input-group">
										<input type="number" name="quota" class="form-control typeahead" placeholder="max data autorisé a uploadé" required="true" />
										<span class="input-group-addon">GB</span>
									</div>
    						</div>
    					</div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">Max utilisateurs</label>
    						<div class="col-sm-8">
    							<div class="input-group">
										<span class="input-group-addon"><i class="entypo-users"></i></span>
										<input type="number" name="max_user" class="form-control typeahead" placeholder="nombre maximum de session a autorisé pour un utilisateur" required="true" />
									</div>
    						</div>
    					</div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">durée</label>
    						<div class="col-sm-8">
    							<div class="input-group">
										<span class="input-group-addon"><i class="entypo-clock"></i></span>
										<input type="number" name="duree" class="form-control typeahead" placeholder="durée de l'abonnement" required="true" />
									</div>
    						</div>
    					</div>

    				
    			</div>
    			<div class="modal-footer">
    			    <button type="submit" class="btn btn-blue"><i class="entypo-check"></i>Ajouter</button>
    				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
    			</div>
    			</form>
    		</div>

    	</div>
    </div>


    <!--********************************************* Modal Modifer abonnement ******************************************* -->
    <div id="EditModal" class="modal fade" role="dialog">
    	<div class="modal-dialog">

    		<!-- Modal content-->
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal">&times;</button>
    				<h4 class="modal-title"><i class="entypo-pencil"></i>  Modifier abonnement</h4>
    			</div>
    			<div class="modal-body">
    			<form class="form-horizontal" action="{{ route('editAbonnement') }}"  method="post">
    					{{ csrf_field() }}
    					<div hidden="true">
    						<input type="number" name="abonnement_ID" id="abonnement_ID" class="form-control typeahead" />
    					</div>

                        <div class="form-group">

                            <label class="control-label col-sm-2" for="email">Titre</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                        <span class="input-group-addon"></span>
                                        <input type="text" name="titre_abonnement" id="titre" class="form-control typeahead" placeholder="donnez un titre a l'abonnement" required="true" />
                                    </div>
                            </div>
                        </div>

    					<div class="form-group">
    						<label class="control-label col-sm-2" for="email">licence</label>
    						<div class="col-sm-8">
    							<div class="input-group">
    								<span class="input-group-addon"><i class="entypo-monitor"></i></span>
    								<input type="number" name="licence" id="licence" class="form-control typeahead" placeholder="nombre d'écran autorisé" required="true" />
    							</div>
    						</div>
    					</div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">Tarification</label>
    						<div class="col-sm-8">
    							<div class="input-group">
    								<input type="number" name="tarification" id="tarification" class="form-control typeahead"  required="true" />
    								<span class="input-group-addon">DA</span>
    							</div>
    						</div>
    					</div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">Quota</label>
    						<div class="col-sm-8">
    							<div class="input-group">
										<input type="number" name="quota" id="quota" class="form-control typeahead" placeholder="max data autorisé a uploadé" required="true" />
										<span class="input-group-addon">GB</span>
									</div>
    						</div>
    					</div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">Max utilisateurs</label>
    						<div class="col-sm-8">
    							<div class="input-group">
										<span class="input-group-addon"><i class="entypo-users"></i></span>
										<input type="number" name="max_user" id="max_user" class="form-control typeahead" placeholder="nombre maximum de session a autorisé pour un utilisateur" required="true" />
									</div>
    						</div>
    					</div>
    					<div class="form-group">

    						<label class="control-label col-sm-2" for="email">durée</label>
    						<div class="col-sm-8">
    							<div class="input-group">
										<span class="input-group-addon"><i class="entypo-clock"></i></span>
										<input type="number" name="duree" id="duree" class="form-control typeahead" placeholder="durée de l'abonnement" required="true" />
									</div>
    						</div>
    					</div>

    				
    			</div>
    			<div class="modal-footer">
    			    <button type="submit" class="btn btn-blue"><i class="entypo-check"></i>Enregistrer</button>
    				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
    			</div>
    			</form>
    		</div>

    	</div>
    </div>

<!--********************************************* Modal Delete abonnement ******************************************* -->
    <div id="deleteModal" class="modal fade" role="dialog">
    	<div class="modal-dialog">

    		<!-- Modal content-->
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal">&times;</button>
    				<h4 class="modal-title"><i class="entypo-trash"></i>  Nouveau abonnement</h4>
    			</div>
    			<div class="modal-body">
    			Veuillez confirmer la suppression, cette action est irréversible !!
    			<form class="form-horizontal" action="{{ route('deleteAbonnement') }}"  method="post">
    					{{ csrf_field() }}
    					<div class="form-group" hidden="true">

    						<div class="col-sm-8">
    							<div class="input-group">
										<input type="number" name="abonnement_ID" id="abonnement_ID_delete" class="form-control typeahead" placeholder="nombre d'écran autorisé" required="true" />
									</div>
    						</div>
    					</div>
    				
    			</div>
    			<div class="modal-footer">
    			    <button type="submit" class="btn btn-blue"><i class="entypo-check"></i>Confirmé</button>
    				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
    			</div>
    			</form>
    		</div>

    	</div>
    </div>
    @endsection('content')

@section('js')

<script type="text/javascript">

  $('.btn[id="edit_abonnement"]').click(function(){

    var id =$(this).attr("data-id");
    var titre =$(this).attr("data-titre");
    var licence =$(this).attr("data-licence");
    var tarification =$(this).attr("data-tarification");
    var quota =$(this).attr("data-quota");
    var max_user =$(this).attr("data-max_user");
    var duree =$(this).attr("data-duree");

    $('#abonnement_ID').val(id); 
    $('#titre').val(titre);
    $('#licence').val(licence);
    $('#tarification').val(tarification);
    $('#quota').val(quota);
    $('#max_user').val(max_user);
    $('#duree').val(duree);

  });


  $('.btn[id="deleteAbonnement"]').click(function(){
  
    var id = $(this).attr("data-abonnement_ID");  
    
    $('#abonnement_ID_delete').val(id);
});

</script>

@endsection