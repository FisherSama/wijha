    @extends('layouts.layout_admin')

    @section('css')
        <link rel="stylesheet" href="{{ asset('js/datatables/datatables.css') }}">
    @endsection



    @section('content')

    <div class="row">

    	<div class="col-sm-12">
    		<div class="table-responsive">

            <table id="demandeQuota" class="table table-bordered datatable">
              <thead>
               <tr>
                 <th>code demande</th>
                 <th>Nom</th>
                 <th>organisme</th>
                 <th>num_telephone</th>
                 <th>etat</th>
                 <th>date</th>
                 <th>option</th>
               </tr>
             </thead>

          </table>
        </div>	

    	</div>

    </div>

<!-- **************************************** Modal more details ************************************* -->
<div id="moreInfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="entypo-info"></i> Détails demande d'un nouveau Quotas</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped">

    <tbody>

      <tr hidden="true">
        <td width="50%"></td>
        <td><input type="text" name="id_Info" id="id_Info"></td>
      </tr>

      <tr>
        <td width="50%">Nom :</td>
        <td id="nom_Info"></td>
      </tr>
      <tr>
        <td>Organisme :</td>
        <td id="organisme_Info"></td>
      </tr>
      <tr>
        <td width="50%">Nombre user :</td>
        <td id="nbr_Info"></td>
      </tr>
      <tr>
        <td width="50%">Nombre afficheur :</td>
        <td id="nbrAfficheur_Info"></td>
      </tr>
      <tr>
        <td width="50%">Nombre Licence :</td>
        <td id="licence_Info"></td>
      </tr>
      <tr>
        <td width="50%">Espace demande :</td>
        <td id="espace_Info"></td>
      </tr>

    </tbody>
  </table>
      </div>
      <div class="modal-footer" id="OperationDdemande">
        <button type="button" class="btn btn-success" onclick="valide()" data-dismiss="modal"><i class="entypo-check"></i> valider</button>
        <button type="button" class="btn btn-danger" onclick="refuse()" data-dismiss="modal"><i class="entypo-cancel"></i>refuser</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>

  </div>  
</div>
    @endsection('content')

@section('js')
<script src="{{ URL::to('js/datatables/datatables.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () {
        var table = $('#demandeQuota').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('alldemande') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
                { "data": "code_demande" },
                { "data": "nom" },
                { "data": "organisme" },
                { "data": "num_telephone" },
                { "data": "etat" },
                { "data": "date" },
                { "data": "option" }
            ]  

        });

   

    });


   

$(document).ready(function() {

       $( document ).on( 'click', '.btn[id="moreInfo"]', function() {
          var id    = $(this).attr('data-id');
          var nom = $(this).attr('data-nom');
          var organisme  = $(this).attr('data-organisme');
          var nbrUser  = $(this).attr('data-nbrUser');
          var nbrAffich = $(this).attr('data-nbrAffich');
          var nbrEspace   = $(this).attr('data-nbrEspace');
          var nbrlicence   = $(this).attr('data-nbrLicence');
          var etatDemande = $(this).attr('data-etatDemande'); 

          if (etatDemande !=='En attente') {
            $('#OperationDdemande').hide();
          }
          else{
            $('#OperationDdemande').show();
          }

          $('#id_Info').empty(' ');
          $('#nom_Info').empty(' ');
           $('#organisme_Info').empty(' ');
          $('#nbr_Info').empty(' ');
          $('#nbrAfficheur_Info').empty(' ');
          $('#espace_Info').empty(' ');
          $('#licence_Info').empty(' ');

          
          $('#id_Info').val(id);
          $('#nom_Info').append(nom);
          $('#organisme_Info').append(organisme);
          $('#nbr_Info').append(nbrUser);
          $('#nbrAfficheur_Info').append(nbrAffich);
          $('#espace_Info').append(nbrEspace);
          $('#licence_Info').append(nbrlicence);

         
       });

   }); 
  

function valide(){
    
   var id_Info = $('#id_Info').val();

   $.ajax({
    type: 'POST',
    url: '/demande/validation',
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    data: {id:id_Info},
    success: function( msg ) {
       $('#demandeQuota').DataTable().ajax.reload();

    }
  });
    
}

function refuse(){

  var id_Info = $('#id_Info').val();

   $.ajax({
    type: 'POST',
    url: '/demande/refuser',
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    data: {id:id_Info},
    success: function( msg ) {
       $('#demandeQuota').DataTable().ajax.reload();

    }
  });
}   
</script>

@endsection   