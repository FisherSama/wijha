<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="{{ asset('images/favicon.ico') }}">


	<title>Digital Signage</title>

	<link rel="stylesheet" href="{{ asset('js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') }} ">
	<link rel="stylesheet" href="{{ asset('css/font-icons/entypo/css/entypo.css') }}">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('css/neon-core.css') }}">
	<link rel="stylesheet" href="{{ asset('css/neon-theme.css') }}">
	<link rel="stylesheet" href="{{ asset('css/neon-forms.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

	<script src="{{ URL::to('js/jquery-1.11.3.min.js') }}"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->


	</head>
	<body class="page-body login-page login-form-fall" data-url="" style="background-color: #071014;">

  <style type="text/css">
  	
  	 input[placeholder], [placeholder], *[placeholder] {
    color: black !important;
    font-size: 13px;
    font-family: initial;
}

  </style>
		<!-- This is needed when you send requests via Ajax -->
		<script type="text/javascript">
			var baseurl = '';
		</script>

		<div class="login-container">



			<div class="login-form">
				<br><br><br><br>
				<center>

					<a href="index.html" class="logo" style="margin-top: 80px;">
						<img src="{{ asset('images/logo.png') }}" width="300" alt="" />
					</a>  

				</center>
				<br><br>
				<div class="login-content">

					<div class="form-login-error">
						<h3>Invalid login</h3>
						<p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p>
					</div>

					<form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" role="form">
						@csrf
						<div class="form-group">
							
							<div class="input-group" style="background-color: white;">
								<div class="input-group-addon">
									<i class="entypo-user"></i>
								</div>
								
								<input
								type="text" class="form-control{{ $errors->has('nom') ? ' is-invalid' : '' }}" name="nom" value="{{ old('nom') }}" id="nom" placeholder="First Name" autocomplete="off" required autofocus/>
								
							</div>
							@if ($errors->has('nom'))
							<span class="invalid-feedback" role="alert">
								<strong style="color: red;">{{ $errors->first('nom') }}</strong>
							</span>
							@endif
							
						</div>

						<div class="form-group">
							
							<div class="input-group" style="background-color: white;">
								<div class="input-group-addon">
									<i class="entypo-user"></i>
								</div>
								
								<input
								type="text" class="form-control{{ $errors->has('prenom') ? ' is-invalid' : '' }}" name="prenom" value="{{ old('prenom') }}" id="nom" placeholder="Family Name" autocomplete="off" required autofocus/>
								
							</div>
							@if ($errors->has('prenom'))
							<span class="invalid-feedback" role="alert">
								<strong style="color: red;">{{ $errors->first('prenom') }}</strong>
							</span>
							@endif
							
						</div>

						<div class="form-group">
							
							<div class="input-group" style="background-color: white;">
								<div class="input-group-addon">
									<i class="entypo-home"></i>
								</div>
								
								<input
								type="text" class="form-control{{ $errors->has('organisme') ? ' is-invalid' : '' }}" name="organisme" value="{{ old('organisme') }}" id="nom" placeholder="Company" autocomplete="off" required autofocus/>
								
							</div>
							@if ($errors->has('organisme'))
							<span class="invalid-feedback" role="alert">
								<strong style="color: red;">{{ $errors->first('organisme') }}</strong>
							</span>
							@endif
							
						</div>

						<div class="form-group" >
							
							<div class="input-group" style="background-color: white;">
								<div class="input-group-addon">
									<i class="entypo-phone"></i>
								</div>
								
								<input
								type="text" class="form-control{{ $errors->has('num_telephone') ? ' is-invalid' : '' }}" name="num_telephone" value="{{ old('num_telephone') }}" id="nom" placeholder="Phone Number" autocomplete="off" required autofocus/>
								
							</div>
							@if ($errors->has('num_telephone'))
							<span class="invalid-feedback" role="alert">
								<strong style="color: red;">{{ $errors->first('num_telephone') }}</strong>
							</span>
							@endif
							
						</div>


						<div class="form-group">

							<div class="input-group" style="background-color: white;">
								<div class="input-group-addon">
									<i class="entypo-mail"></i>
								</div>

								<input
								type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" id="email" placeholder="{{ __('messages.email') }}" autocomplete="off" required autofocus/>

							</div>
							@if ($errors->has('email'))
							<span class="invalid-feedback" role="alert">
								<strong style="color: red;">{{ $errors->first('email') }}</strong>
							</span>
							@endif

						</div>

						<div class="form-group">

							<div class="input-group" style="background-color: white;">
								<div class="input-group-addon">
									<i class="entypo-key"></i>
								</div>

								<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  autocomplete="off" placeholder="{{ __('messages.password') }}" required/>
							</div>
							@if ($errors->has('password'))
							<span class="invalid-feedback" role="alert">
								<strong style="color: red;">{{ $errors->first('password') }}</strong>
							</span>
							@endif

						</div>

						<div class="form-group">

							<div class="input-group" style="background-color: white;">
								<div class="input-group-addon">
									<i class="entypo-key"></i>
								</div>

								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="off" placeholder="{{ __('messages.password') }}" required/>
							</div>

						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-info btn-block btn-login" style="background-color: #F7CA11;">
								<i class="entypo-login"></i>
								{{ __('Register') }}
							</button>
						</div>


				<!-- 
				
				You can also use other social network buttons
				<div class="form-group">
				
					<button type="button" class="btn btn-default btn-lg btn-block btn-icon icon-left twitter-button">
						Login with Twitter
						<i class="entypo-twitter"></i>
					</button>
					
				</div>
				
				<div class="form-group">
				
					<button type="button" class="btn btn-default btn-lg btn-block btn-icon icon-left google-button">
						Login with Google+
						<i class="entypo-gplus"></i>
					</button>
					
				</div> -->
				
			</form>
			
			
		</div>
		
	</div>
	
</div>


<!-- Bottom scripts (common) --> 
<script src="{{ URL::to('js/gsap/TweenMax.min.js') }}"></script>
<script src="{{ URL::to('js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') }}"></script>
<script src="{{ URL::to('js/bootstrap.js') }}"></script>
<script src="{{ URL::to('js/joinable.js') }}"></script>
<script src="{{ URL::to('js/resizeable.js') }}"></script>
<script src="{{ URL::to('js/neon-api.js') }}"></script>
<script src="{{ URL::to('js/jquery.validate.min.js') }}"></script>
<script src="{{ URL::to('js/neon-login.js') }}"></script>


<!-- JavaScripts initializations and stuff -->
<script src="{{ URL::to('js/neon-custom.js') }}"></script>


<!-- Demo Settings -->
<script src="{{ URL::to('js/neon-demo.js') }}"></script>

</body>
</html>