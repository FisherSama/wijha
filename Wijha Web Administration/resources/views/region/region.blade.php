  @extends('layouts.layout_admin')
  @section('content')

  @section('css')
  

    
  @endsection('css')

  <style type="text/css">
  	
     #global{
           
          margin: 5px; 
          margin-top:20px;
     	  /*border-style: solid;*/
          /*border-color: #EEEEEE;*/
          height: auto;
          /*background-color: #FAFAFA;*/
          /*border-width: 1px;*/
     }

   


     #global_region{
	
	     	  border-style: solid;
	          border-color: #616161;
	          border-width: thin;
	          padding: 1px;
	        
	     }

	  #config{
	  	padding: 20px;
	  	
	  }  


      #drawing {
     	
     	max-height: 700px;
        z-index: 9;
     	/*overflow:scroll;*/
     	padding: 1px;
     	

     }

     #create_region , #delete_region,#close_animation {
     	cursor: pointer;
     }





	  div[id^="region"]{

		    cursor: move;
		    z-index: 10;
		    background-color:#ECEFF1;
		    position: absolute;
		  

	  }  

  </style>



   <div class="row">
       <div class="col-md-2"></div>
       <div class="col-md-10">

       	      <div class="form-inline">


       	             <div class="form-group">
					    <label for="email">Layout Name:</label>
					    <input type="text" class="form-control" id="name_layout" placeholder="Name">
					 </div>


					  <div class="form-group">
					    <label for="email">Height:</label>
					    <input type="number" class="form-control" id="height">
					  </div>

					  <div class="form-group">
					    <label for="pwd">Width:</label>
					    <input type="number" class="form-control" id="width">
					  </div>
					  
					  <button type="text" class="btn btn-primary btn-large" id="create">Create</button>
			 </div> 

       </div>
       
   </div>

   <hr>

   <div class="row" style="margin: -10px;" id="settings" hidden="true"> 
   	  <div class="col-md-7"></div>
   	  <div class="col-md-5" style="float: left;">
   	  	 
   	  	  <ul class="pagination">
		   <li><span href="#" id="create_region"><i class="entypo-plus"></i> New region</span></li>
		   <li><span id="delete_region"><i class="entypo-trash"></i> Delete last</span></li>
		   <li><span id="close_animation"><i class="entypo-check"></i>End Layout</span></li>
		</ul>

   	  </div>
   </div>



   <div class="row" id="global" hidden="">
      <div class="col-md-12 " id="drawing">
      </div> 	
   </div>


     
  @endsection('content')

   @section('js_files')

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



    <script src="{{ URL::to('js/jquery-collision.js') }}"></script>



   @endsection('js_files')




   @section('js')

   <script type="text/javascript">
   	
    $('#create').click(function(){



	     $('#drawing').html('');

	     var height=$('#height').val();
	     var width=$('#width').val();

	     $('#global').show();
	     $('#settings').show();

	     

	    
	     var div="<div id='global_region'></div>"
	     $('#drawing').append(div);
	   
	     $('#global_region').css('height',height);
	     $('#global_region').css('width',width);

	     $("#global_region").draggable({ containment: '#drawing'});

         /***********************************************************/

       /* var div3="<div id='region2' style='height:100px;width:100px;left:200px;top:200px;'></div>";
        var div4="<div id='region3' style='height:100px;width:100px;'></div>";

        $('#global_region').append(div3);
        $('#global_region').append(div4);


         $( "#region2").draggable({ containment: '#global_region'});
         $( "#region3").draggable({ containment: '#global_region'});*/


        /* var colliders_selector = "#region2";
		 var obstacles_selector = "#region3";
		 var hits = $(colliders_selector).collision(obstacles_selector);*/

		 /*$("#region2").draggable( { obstacle: "#region3" } );
		 $("#region3").draggable( { obstacle: "#region2" } );*/


         //var hit_list = $("#region2").collision("#region3");

         //console.log(hits);

         /* $("#region1").draggable( { obstacle: "#region2" } );
	      $("#region2").draggable( { obstacle: "region1" } );*/


    })


     var i=1;

    $('#create_region').click(function(){

        var div2="<div id='region"+i+"' style='height:100px;width:100px;'></div>";

        $('#global_region').append(div2);
        $( "#region"+i).resizable({ containment: '#global_region'});
        $( "#region"+i).draggable({ containment: '#global_region'});

    
       
       
       
       
    	i++;

	   $('div[id^="region"]').css('border-style','solid');
	   $('div[id^="region"]').css('border-color','#90A4AE');
	   $('div[id^="region"]').css('border-width','thin');

	  

    })


    $('#delete_region').click(function(){
         i--;
         $('#region'+i).remove();


    })


    $('#close_animation').click(function(){

       
         var global_height=$('#global_region').height();
         var global_width=$('#global_region').width();
          
         var all_layout={}; 
         
         var layout = {height:global_height, width:global_width, top:0 , left:0};
         all_layout.layout=layout;

         var regions=[];


        var x=1;

    	for(x=1;x<i;x++){

          

    	  var button='<a href="/region/edit/'+x+'"><button class="btn btn-white"><i class="entypo-edit"></i>Edit region '+x+' </button></a>';

		  			

           $("#region"+x).draggable('disable');
           $("#region"+x).resizable('disable');

           $("#region"+x).append(button);

           

           var position=$("#region"+x).position();

           var left=position.left;
           var top=position.top;



           var height=$("#region"+x).height();
           var width=$("#region"+x).width();

           var region={height:height ,width:width,top:top, left:left};
           regions.push(region);

         
    	}

        all_layout.regions=regions;
    	  var myJSON = JSON.stringify(all_layout);
        console.log(myJSON);
        
    })






   </script>

   @endsection('js')


  



