  @extends('layouts.layout_admin')
  @section('content')

  <style type="text/css">

  	.wrapper {
	background-color: black;
	color: white;
	display: flex;
	flex-flow: row nowrap;
	min-height: 100%;
	width: 100%;
	border-style: solid;
    border-width: 1px;
    border-color: #607D8B;
    border-radius: 8;

}

	.wrapper2 {
	
	color: white;
	display: flex;
	flex-flow: row nowrap;

	width: 100%;
	border-style: solid;
    border-width: 1px;
    border-color: #607D8B;
    border-radius: 8;
    max-height: 80px;
   
}


.icon {

	align-self: center;
	color: #607D8B;
	background-color: #263238;
	flex: 1 1 auto;
	font-size: 32px;
	padding: 20px;
	width: 50%;
}

.icon2 {

	align-self: center;
	color: #607D8B;
	background-color: #263238;
	flex: 1 1 auto;
	font-size: 32px;
	padding: 20px;
	width: 20%;
	max-height: 80px;
}

.content {
	background-color: white;
	color: black;
	flex: 12 1 auto;
	padding: 20px;
}


#time {

	border-style: solid;
    border-width: 1px;
    border-color: #cfd8dc;
    border-radius: 5px;
    margin-top:55px;
    height: 70px;
    padding: 20px;
    text-align: center;
    font-size: 15px;
     box-shadow:  1px;
}

#content {

	border-style: solid;
    border-width: 1px;
    border-color: #cfd8dc;
    border-radius: 5px;
    margin-top:5px;
    max-height: 360px;
    padding: 20px;
    text-align: center;
    font-size: 15px;
    box-shadow:  1px;
    overflow-x: scroll;
}

#content2 {

	border-style: solid;
    border-width: 1px;
    border-color: #cfd8dc;
    border-radius: 5px;
    margin-top:5px;
    max-height: 360px;
    padding: 20px;

    font-size: 15px;
    box-shadow:  1px;
    overflow-x: scroll;
}

  </style>

    <div class="row">
          <div class="col-md-7"> 
             <div class="col-md-12" id="time"> <b> Total Time </b> : 01:20 min</div>
             <div class="col-md-12" id="content"></div>
             
          </div>
          <div class="col-md-5"> 
            
               	<div class="form-group">
								<label class="col-sm-3 control-label" style="margin-top: 7px;">Search</label>
								
								<div class="col-sm-9">
									<div class="input-group">
										<span class="input-group-addon"><i class="entypo-search"></i></span>
										<input type="text" name="typeahead_local" class="form-control typeahead" />
									</div>
								</div>
				  </div>


			<div class="col-md-12">
				
				<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
					<li class="active">
						<a href="#home" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-home"></i></span>
							<span class="hidden-xs">All</span>
						</a>
					</li>
					<li>
						<a href="#profile" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">Images</span>
						</a>
					</li>
					<li>
						<a href="#messages" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-mail"></i></span>
							<span class="hidden-xs">Videos</span>
						</a>
					</li>
					<li>
						<a href="#settings" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-cog"></i></span>
							<span class="hidden-xs">Docs</span>
						</a>
					</li>
				</ul>
				
				<div class="tab-content" style="height: 400px;">

              

					<div class="tab-pane active" id="home">
						 <div class="wrapper" style="margin: 5px;">
									<div class="icon">
										<img src="{{ asset('images/pictures.png') }}" width="80" height="80" alt="" />
									</div>
									<div class="content">
										<h4>Picture 1</h4>
										<p>This is an example application's description text.</p>
									</div>
						  </div>

						   <div class="wrapper" style="margin: 5px;">
									<div class="icon">
										<img src="{{ asset('images/pictures.png') }}" width="80" height="80" alt="" />
									</div>
									<div class="content">
										<h4>Picture 2</h4>
										<p>This is an example application's description text.</p>
									</div>
						  </div>

						  

						   <div class="wrapper" style="margin: 5px;">
									<div class="icon">
										<img src="{{ asset('images/pictures.png') }}" width="80" height="80" alt="" />
									</div>
									<div class="content">
										<h4>Picture 1</h4>
										<p>This is an example application's description text.</p>
									</div>
						  </div>
					</div>
					<div class="tab-pane" id="profile">
						
							
					</div>

					<div class="tab-pane" id="messages">
							
							
					</div>
					
					<div class="tab-pane" id="settings">
							
							
					</div>
				</div>
				
				
			</div>
             
          </div>
    </div>


  @endsection('content')

   @section('js_files')


    <script src="{{ URL::to('js/select2/select2-bootstrap.css') }}"></script>
    <script src="{{ URL::to('js/select2/select2.css') }}"></script>



   @endsection('js_files')


   @section('js')
    <script type="text/javascript">
    	
    	$('.wrapper').click(function(){
    		

    	  var content='<div class="wrapper2" style="margin: 5px;"><div class="icon2"><img src="{{ asset("images/pictures.png") }}" width="40" height="40" alt="" /></div><div class="content"><h4>Picture 1</h4></div></div>';
			

		   //console.log(content);		

		 $('#content').append(content);			
    	})
    </script>

   @endsection('js')








