<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="assets/images/favicon.ico">

	<title>Stream | Display </title>
     
	<link rel="stylesheet" href="{{ asset('js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-icons/entypo/css/entypo.css') }}">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('css/neon-core.css') }}">
	<link rel="stylesheet" href="{{ asset('css/neon-theme.css') }}">
	<link rel="stylesheet" href="{{ asset('css/neon-forms.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

	<script src="{{ URL::to('js/jquery-1.11.3.min.js') }}"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<bozdy class="page-body  page-fade gray" data-url="http://neon.dev">
  <!-- page-fade gray  -->
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="index.html">
						<img src="{{ asset('images/logo_stream_white.png') }}" width="120" alt="" />
					</a>  
				</div>  

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				@if( Auth::user()->role != 'simple_user')
				<li class="opened active">
					<a href="index.html">
						<i class="entypo-gauge"></i>
						<span class="title">{{ __('messages.dashboard') }}</span>
					</a>
				</li>
				@endif
				<li>
					<a href="layout-api.html">
						<i class="entypo-calendar"></i>
						<span class="title">{{ __('messages.planning') }}</span>
					</a>

				</li>

				<li class="has-sub">
					<a href="ui-panels.html">
						<i class="entypo-doc"></i>
						<span class="title">{{ __('messages.layout') }}</span>
					</a>
					<ul>
						<li>
							<a href="ui-panels.html">
							   <i class="entypo-inbox"></i>
								<span class="title">Compagne</span>
							</a>
						</li>
						<li>
							<a href="ui-tiles.html">
							<i class="entypo-inbox"></i>
								<span class="title">Layouts</span>
							</a>
						</li>

					</ul>
				</li>
				<li class="has-sub">
					<a href="mailbox.html">
						<i class="entypo-monitor"></i>
						<span class="title">{{ __('messages.display') }}</span>
						<span class="badge badge-secondary">8</span>
					</a>
					<ul>
						<li>
							<a href="mailbox.html">
								<i class="entypo-inbox"></i>
								<span class="title">Inbox</span>
							</a>
						</li>
						<li>
							<a href="mailbox-compose.html">
								<i class="entypo-pencil"></i>
								<span class="title">Compose Message</span>
							</a>
						</li>
						<li>
							<a href="mailbox-message.html">
								<i class="entypo-attach"></i>
								<span class="title">View Message</span>
							</a>
						</li>
					</ul>
				</li>
				@if( Auth::user()->role != 'simple_user')
				<li class="has-sub">
					<a href="forms-main.html">
						<i class="entypo-video"></i>
						<span class="title">{{ __('messages.library') }}</span>
					</a>
					<ul>
						<li>
							<a href="forms-main.html">
							   <i class="entypo-pencil"></i>
								<span class="title">Basic Elements</span>
								
							</a>
						</li>
						<li>
							<a href="forms-advanced.html">
							<i class="entypo-pencil"></i>
								<span class="title">Advanced Plugins</span>

							</a>
						</li>
						
					</ul>
				</li>
				@endif
				@if( Auth::user()->role == 'super_admin')
				<li class="has-sub">
					<a href="tables-main.html">
						<i class="entypo-cog"></i>
						<span class="title">{{ __('messages.administrator') }}</span>
					</a>
					<ul>
						<li>
							<a href="tables-main.html">
								<span class="title">Basic Tables</span>
							</a>
						</li>
						<li>
							<a href="tables-datatable.html">
								<span class="title">Data Tables</span>
							</a>
						</li>
					</ul>
				</li>
				@endif
				<li class="has-sub">
					<a href="extra-icons.html">
						<i class="entypo-list"></i>
						<span class="title">{{ __('messages.logs') }}</span>
						
					</a>
					<ul>
	
						<li>
							<a href="extra-login.html">
								<span class="title">Login</span>
							</a>
						</li>
						<li>
							<a href="extra-register.html">
								<span class="title">Register</span>
							</a>
						</li>


					</ul>
				</li>
				@if( Auth::user()->role == 'super_admin')
				<li>
					<a href="charts.html">
						<i class="entypo-chart-area"></i>
						<span class="title">{{ __('messages.statistics') }}</span>
					</a>
				</li>
				@endif


					<li class="has-sub">
					<a href="extra-icons.html">
						<i class="entypo-user"></i>
						<span class="title">{{ __('messages.users') }}</span>
					</a>
					<ul>
	
						<li>
							<a href="/my_groups">
								<span class="title">{{ __('messages.users_group') }}</span>
							</a>
						</li>
						<li>
							<a href="groups/members/">
								<span class="title">{{ __('messages.users_list') }}</span>
							</a>
						</li>


					</ul>
				</li>

			</ul>
			
		</div>

	</div>

	<div class="main-content">
				
		<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
		
				<ul class="user-info pull-left pull-none-xsm">
		
					<!-- Profile Info -->
					<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="{{ asset('images/thumb-1@2x.png') }}" alt="" class="img-circle" width="44" />
							{{ Auth::user()->nom }} {{ Auth::user()->prenom }}

  
						</a>
		
						<ul class="dropdown-menu">
		
							<!-- Reverse Caret -->
							<li class="caret"></li>
		
							<!-- Profile sub-links -->
							<li>
								<a href="extra-timeline.html">
									<i class="entypo-user"></i>
									Edit Profile
								</a>
							</li>
		
							<li>
								<a href="mailbox.html">
									<i class="entypo-mail"></i>
									Inbox
								</a>
							</li>
		
							<li>
								<a href="extra-calendar.html">
									<i class="entypo-calendar"></i>
									Calendar
								</a>
							</li>
		
							<li>
								<a href="#">
									<i class="entypo-clipboard"></i>
									Tasks
								</a>
							</li>
						</ul>
					</li>
		
				</ul>
				
				<ul class="user-info pull-left pull-right-xs pull-none-xsm">
		
					<!-- Raw Notifications -->
					<li class="notifications dropdown">
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="entypo-attention"></i>
							<span class="badge badge-info">6</span>
						</a>
		
						<ul class="dropdown-menu">
							<li class="top">
								<p class="small">
									<a href="#" class="pull-right">Mark all Read</a>
									You have <strong>3</strong> new notifications.
								</p>
							</li>
							
							<li>
								<ul class="dropdown-menu-list scroller">
									<li class="unread notification-success">
										<a href="#">
											<i class="entypo-user-add pull-right"></i>
											
											<span class="line">
												<strong>New user registered</strong>
											</span>
											
											<span class="line small">
												30 seconds ago
											</span>
										</a>
									</li>
									
								</ul>
							</li>
							
							<li class="external">
								<a href="#">View all notifications</a>
							</li>
						</ul>
		
					</li>
		
					<!-- Message Notifications -->
					<li class="notifications dropdown">
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="entypo-mail"></i>
							<span class="badge badge-secondary">10</span>
						</a>
		
						<ul class="dropdown-menu">
							<li>
								<form class="top-dropdown-search">
									
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Search anything..." name="s" />
									</div>
									
								</form>
								
								<ul class="dropdown-menu-list scroller">
									<li class="active">
										<a href="#">
											<span class="image pull-right">
												<img src="{{ asset('images/thumb-1@2x.png') }}" width="44" alt="" class="img-circle" /> 
											</span>
											
											<span class="line">
												<strong>Luc Chartier</strong>
												- yesterday
											</span>
											
											<span class="line desc small">
												This ain’t our first item, it is the best of the rest.
											</span>
										</a>
									</li>
								
								</ul>
							</li>
							
							<li class="external">
								<a href="mailbox.html">All Messages</a>
							</li>
						</ul>
		
					</li>
		
					<!-- Task Notifications -->
					<li class="notifications dropdown">
		
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="entypo-list"></i>
							<span class="badge badge-warning">1</span>
						</a>
		
						<ul class="dropdown-menu">
							<li class="top">
								<p>You have 6 pending tasks</p>
							</li>
							
							<li>
								<ul class="dropdown-menu-list scroller">
									<li>
										<a href="#">
											<span class="task">
												<span class="desc">Procurement</span>
												<span class="percent">27%</span>
											</span>
										
											<span class="progress">
												<span style="width: 27%;" class="progress-bar progress-bar-success">
													<span class="sr-only">27% Complete</span>
												</span>
											</span>
										</a>
									</li>

								</ul>
							</li>
							
							<li class="external">
								<a href="#">See all tasks</a>
							</li>
						</ul>
		
					</li>
		
				</ul>
		
			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">
		
					<!-- Language Selector -->
					<li class="dropdown language-selector">

					    
		
						{{ __('messages.language') }}: &nbsp;
						<a href="" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">


						 @if(session('lng') =="en") 
					     <img src="{{ asset('images/flags/flag-uk.png') }}" width="16" height="16" />
						 @endif

						 @if(session('lng') =="ar") 
					     <img src="{{ asset('images/flags/flag-al.png') }}" width="16" height="16" />
						 @endif

						 @if(session('lng') =="fr") 
					     <img src="{{ asset('images/flags/flag-fr.png') }}" width="16" height="16" />
						 @endif

						 @if(session('lng') =="it") 
					     <img src="{{ asset('images/flags/flag-italy.png') }}" width="16" height="16" />
						 @endif

						 @if(session('lng') =="es") 
					     <img src="{{ asset('images/flags/flag-es.png') }}" width="16" height="16" />
						 @endif

						  @if(session('lng') =="pr") 
					     <img src="{{ asset('images/flags/flag-portugal.png') }}" width="16" height="16" />
						 @endif



						 </a> 
		
						<ul class="dropdown-menu pull-right">

							<li class="active">
								<a href="/language/en"> 
									<img src="{{ asset('images/flags/flag-uk.png') }}" width="16" height="16" />
									<span>English</span>
								</a>
							</li>
							<li>
								<a href="/language/ar"> 
									<img src="{{ asset('images/flags/flag-al.png') }}" width="16" height="16" />
									<span>العربية</span>
								</a>
							</li>
							<li>
								<a href="/language/fr">
									<img src="{{ asset('images/flags/flag-fr.png') }}" width="16" height="16" />
									<span>Français</span>
								</a>
							</li>	

							<li>
								<a href="/language/it"> 
									<img src="{{ asset('images/flags/flag-italy.png') }}" width="16" height="16" />
									<span>Italy</span>
								</a>
							</li>
														
							<li>
								<a href="/language/es">  
									<img src="{{ asset('images/flags/flag-es.png') }}" width="16" height="16" />
									<span>Español</span>
								</a>
							</li>
							<li>
								<a href="/language/pr">  
									<img src="{{ asset('images/flags/flag-portugal.png') }}" width="16" height="16" />
									<span>Portugal</span>
								</a>
							</li>
						</ul>
		
					</li> 
		
					<li class="sep"></li>
		
					

		
					<li class="sep"></li>
		
					<li>
						<a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
							{{ __('messages.log_out') }} <i class="entypo-logout right"></i>
						</a>
					</li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
	   @if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@if ( session()->has('success'))
		<div class="alert alert-success">
			<ul>
				<li>{{ session('success') }}</li>
			</ul>
		</div>
		@endif
		@if ( session()->has('warning'))
		<div class="alert alert-warning">
			<ul>
				<li>{{ session('warning') }}</li>
			</ul>
		</div>
		@endif
		
		 @yield('content')


		<!-- Footer -->
		<footer class="main">
			
			&copy; 2015 <strong>Neon</strong> Admin Theme by <a href="http://laborator.co" target="_blank">Laborator</a>
		
		</footer>
	</div>
	

</div>


	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="{{ asset('js/jvectormap/jquery-jvectormap-1.2.2.css') }}">
	<link rel="stylesheet" href="{{ asset('js/rickshaw/rickshaw.min.css') }}">

	<!-- Bottom scripts (common) -->
	<script src="{{ URL::to('js/gsap/TweenMax.min.js') }}"></script>
	<script src="{{ URL::to('js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') }}"></script>
	<script src="{{ URL::to('js/bootstrap.js') }}"></script>
	<script src="{{ URL::to('js/joinable.js') }}"></script>
	<script src="{{ URL::to('js/resizeable.js') }}"></script>
	<script src="{{ URL::to('js/neon-api.js') }}"></script>
	<script src="{{ URL::to('js/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>


	<!-- Imported scripts on this page -->
	<script src="{{ URL::to('js/jvectormap/jquery-jvectormap-europe-merc-en.js') }}"></script>
	<script src="{{ URL::to('js/jquery.sparkline.min.js') }}"></script>
	<script src="{{ URL::to('js/rickshaw/vendor/d3.v3.js') }}"></script>
	<script src="{{ URL::to('js/rickshaw/rickshaw.min.js') }}"></script>
	<script src="{{ URL::to('js/raphael-min.js') }}"></script>
	<script src="{{ URL::to('js/morris.min.js') }}"></script>
	<script src="{{ URL::to('js/toastr.js') }}"></script>
	<script src="{{ URL::to('js/fullcalendar/fullcalendar.min.js') }}"></script>
	<script src="{{ URL::to('js/neon-chat.js') }}"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="{{ URL::to('js/neon-custom.js') }}"></script>


	<!-- Demo Settings -->
	<script src="{{ URL::to('js/neon-demo.js') }}"></script>
    @yield('js')

</body>
</html>