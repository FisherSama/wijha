    @extends('layouts.layout_admin')

    @section('content')


    <div class="gallery-env">
    
      <div class="row">
      
        <div class="col-sm-12 image-categories">
                    
          <div class="row">
            <div class="col-sm-10">
              <h3>
            Gestion des Médias</h3>
            </div>
            <div class="col-sm-2">
              <h3>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
            <i class="entypo-upload"></i> Open Modal</button></h3>
            </div>
          </div>
           
        </div>
      
      </div>
    
      <div class="row">
            <div class="col-md-12">
      
        <div class="panel minimal minimal-gray">
          
          <div class="panel-heading">
            <div class="panel-title"><h4>Minimal Panel</h4></div>
            <div class="panel-options">
              
              <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">First Tab</a></li>
                <li><a href="#profile-2" data-toggle="tab">Second Tab</a></li>
              </ul>
            </div>
          </div>
          
          <div class="panel-body">
            
            <div class="tab-content">
              <div class="tab-pane active" id="profile-1">

                <div class="col-sm-2 col-xs-4" data-tag="1d">

                  <article class="image-thumb">

                    <a href="#" class="image">
                      <img src="images/album-image-1.jpg" />
                    </a>

                    <div class="image-options">
                      <a href="#" class="edit"><i class="entypo-pencil"></i></a>
                      <a href="#" class="delete"><i class="entypo-cancel"></i></a>
                    </div>

                  </article>

                </div>


              </div>
              
              <div class="tab-pane" id="profile-2">
                <div class="col-sm-3 col-xs-4" data-tag="1d">

                  <article class="album">

                    <header>

                      <a href="extra-gallery-single.html">
                        <img src="images/album-image-1.jpg" />
                      </a>

                      <a href="#" class="album-options">
                        <i class="entypo-monitor"></i>
                        visualisé
                      </a>
                    </header>

                    <section class="album-info">
                      <h3><a href="extra-gallery-single.html">Album Title</a></h3>
                    </section>

                    <footer>

                      <div class="album-images-count">
                        <i class="entypo-picture"></i>
                        55
                      </div>

                      <div class="album-options">
                        <a href="#" style="color: blue;">
                          <i class="entypo-pencil"></i> edit
                        </a>

                        <a href="#" style="color: red;">
                          <i class="entypo-trash"></i> delete
                        </a>
                      </div>

                    </footer>

                  </article>

                </div>




              </div>
            </div>
            
          </div>
          
        </div>
        
      </div>
      

        
      </div>
    
    </div>


    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Modal Header</h4>
          </div>
          <div class="modal-body">
            
            <div class="form-group">
              <label class="col-sm-3 control-label">Image Upload</label>
              
              <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                  <img src="http://placehold.it/200x150" alt="...">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                <div>
                  <span class="btn btn-white btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="..." accept="image/*">
                  </span>
                  <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
              </div>
              
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>


    @endsection('content')

@section('js')
  <script src="js/fileinput.js"></script>
<script type="text/javascript">

  
</script>

@endsection