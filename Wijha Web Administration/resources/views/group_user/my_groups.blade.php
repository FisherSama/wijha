  @extends('layouts.layout_admin')
  
  @section('css')
  
  <link rel="stylesheet" href="{{ asset('js/datatables/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('js/select2/select2-bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('js/select2/select2.css') }}">

  @endsection('css')


  @section('content')

   <div class="row">
    <div class="col-sm-10"></div>
    <div class="col-sm-2" style="float: right;">

    	<button type="button" class="btn btn-primary btn-icon icon-left" data-toggle="modal" data-target="#createModal"><i class="entypo-plus" ></i> {{ __('messages.new_group') }}</button>
    </div>
  </div>

   <br> 
 <table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr>

					<th data-hide="phone">{{ __('messages.name_group') }}</th>
					<th>{{ __('messages.date_creation') }}</th>
					<th data-hide="phone">{{ __('messages.operations') }}</th>
					<th> {{ __('messages.consulte') }} </th>
          <th> {{ __('messages.permission') }} </th>
					
					
				</tr>
			</thead>
			<tbody>
              
             
			    @foreach($my_groups as $groupe)

							<tr class="odd gradeX">
								<td>
								<center>
								{{$groupe->nom_groupe}}
								</center>
								</td>
								<td>
								  <center>
								  {{$groupe->created_at}}
								 </center>
								</td>
								<td> 
								<center>

								<button type="button" class="btn btn-success btn-xs"  onclick="updateGroup({{$groupe->id}},'{{$groupe->nom_groupe}}')" data-toggle="modal" data-target="#updateModal">
								<i class="entypo-menu"></i> {{ __('messages.edit') }}
							    </button>

							    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#deleteModal" onclick="deleteGroup({{$groupe->id}})">
								   <i class="entypo-trash"></i> {{ __('messages.delete') }}
						    	</button>
							    </center>
							   </td>

							   <td> 
							     <center>
							     <a href="/groups/consulte/{{$groupe->id}}">{{ __('messages.consulte') }} </a> 
							    </center>
							    </td>

                   <td> 
                   <center>
                   <a href="/groups/permission/{{$groupe->id}}">{{ __('messages.permission') }} </a> 
                  </center>
                  </td>

							</tr>

				@endforeach		
			
			
			</tbody>
			<tfoot>
				<tr>
				  <th data-hide="phone">{{ __('messages.name_group') }}</th>
          <th>{{ __('messages.date_creation') }}</th>
          <th data-hide="phone">{{ __('messages.operations') }}</th>
          <th> {{ __('messages.consulte') }} </th>
          <th> {{ __('messages.permission') }} </th>
				</tr>
			</tfoot>
		</table>





<!--  ***********    Modal Create new group   **************** -->



  <div id="createModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

  		<!-- Modal content-->

  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal">&times;</button>
  				<h4 class="modal-title"><i class="entypo-plus"></i>  {{ __('messages.new_group') }}</h4>
  			</div>

  			<div class="modal-body">
  			<form class="form-horizontal" action="/groups/add" method="post">
  					{{ csrf_field() }}
  					<div class="form-group">

  					<label class="control-label col-sm-3" for="email">{{ __('messages.name_group') }}</label>
  						<div class="col-sm-9">
  						  <div class="input-group">
							<span class="input-group-addon"><i class="entypo-info"></i></span>
                             <input type="text" name="nom_groupe" class="form-control typeahead"  />
						  </div>
  						</div>
  					</div>


            <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.name_group') }}</label>
              <div class="col-sm-9">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-users"></i></span>
                 <select class="form-control typeahead" name="groupe" >

                     <option value="">  </option>
                     <!--  @foreach($my_groups as $groupe)
                       <option value="{{$groupe->id}}"> {{$groupe->nom_groupe}}</option>
                      @endforeach  -->

                 </select>
              </div>
              </div>
            </div>

		  			<div class="modal-footer">

		  			  <button type="submit" class="btn btn-primary">{{ __('messages.add') }}
		  			  </button>
		  			</div>

  			 </form>
  			</div>

  			
  		</div>


  	</div>
  </div>

<!--  ***********    END  Modal Create new group   **************** -->





<!--  ***********    Modal Delete group   **************** -->



  <div id="deleteModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

  		<!-- Modal content-->

  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal">&times;</button>
  				<h4 class="modal-title"> {{ __('messages.delete') }}</h4>
  			</div>

  			<div class="modal-body">
  			<form class="form-horizontal" action="/groups/delete" method="post">
  					{{ csrf_field() }}
  					<div class="form-group" hidden="true">

  					<label class="control-label col-sm-2" for="email">{{ __('messages.name_group') }}</label>
  						<div class="col-sm-8">
  						  <div class="input-group">
							<span class="input-group-addon"><i class="entypo-info"></i></span>
                             <input type="text" name="id_groupe" id="id_groupe" class="form-control typeahead" required="true" />
						  </div>
  						</div>
  					</div>


            <h3> <span>  {{ __('messages.confirmation_delete') }} </span>  </h3>
            

		  			<div class="modal-footer">

		  			  <button type="submit" class="btn btn-primary">{{ __('messages.delete') }}
		  			  </button>
		  			</div>

  			 </form>
  			</div>

  			
  		</div>


  	</div>
  </div>

<!--  ***********    END  Modal Create delete group   **************** -->

<!--  ***********    Modal Update group   **************** -->



  <div id="updateModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

  		<!-- Modal content-->

  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal">&times;</button>
  				<h4 class="modal-title"><i class="entypo-plus"></i>  {{ __('messages.update') }}</h4>
  			</div>

  			<div class="modal-body">
  			<form class="form-horizontal" action="/groups/update" method="post">
  					{{ csrf_field() }}
  					<div class="form-group" hidden="true">

  					<label class="control-label col-sm-2" for="email">{{ __('messages.name_group') }}</label>
  						<div class="col-sm-8">
  						  <div class="input-group">
							<span class="input-group-addon"><i class="entypo-info"></i></span>
                             <input type="text" name="id_groupe" id="id_groupe_up" class="form-control typeahead" required="true" />
						  </div>
  						</div>
  					</div>

  					<div class="form-group">

  					<label class="control-label col-sm-2" for="email">{{ __('messages.name_group') }}</label>
  						<div class="col-sm-8">
  						  <div class="input-group">
							<span class="input-group-addon"><i class="entypo-info"></i></span>
                             <input type="text" name="nom_groupe" id="nom_groupe_up" class="form-control typeahead" required="true" />
						  </div>
  						</div>
  					</div>

		  			<div class="modal-footer">

		  			  <button type="submit" class="btn btn-primary">{{ __('messages.update') }}
		  			  </button>
		  			</div>

  			 </form>
  			</div>

  			
  		</div>


  	</div>
  </div>

<!--  ***********    END  Modal Create delete group   **************** -->




  @endsection('content')

  @section('js_files')

  <script src="{{ URL::to('js/datatables/datatables.js') }}"></script>
  <script src="{{ URL::to('js/select2/select2.min.js') }}"></script>
  <script src="{{ URL::to('js/neon-chat.js') }}"></script>

  @endsection('js_files')

  @section('js')
  <script type="text/javascript">
		jQuery( document ).ready( function( $ ) {
			
		   var $table1 = jQuery( '#table-1' );
		
			// Initialize DataTable
			$table1.DataTable( {
				"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
				"bStateSave": true
			});
			
			// Initalize Select Dropdown after DataTables is created
			$table1.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
				minimumResultsForSearch: -1
			});
		} );
		</script>

       <script type="text/javascript">
       	   
       	   function deleteGroup(id_groupe){
       	   	$('#id_groupe').val(id_groupe);
       	   }


       	   function updateGroup(id_groupe,nom_groupe){

       	   	console.log(id_groupe+'--'+nom_groupe);
       	   	$('#nom_groupe_up').val(nom_groupe);
       	   	$('#id_groupe_up').val(id_groupe);

       	   }

       </script>
  @endsection('js')
  

