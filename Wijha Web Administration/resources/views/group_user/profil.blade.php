  @extends('layouts.layout_admin')
  @section('content')

 
 	<div class="profile-env">

			<header class="row">
				
				<div class="col-sm-2">
					
					<a href="#" class="profile-picture">
						<img src="{{ asset('images/profile-picture.png') }}" class="img-responsive img-circle" />
					</a>
					
				</div>
				
				<div class="col-sm-7">
					
					<ul class="profile-info-sections">
						<li>
							<div class="profile-name">
								<strong>
									<a href="#">{{$user->nom}} {{$user->prenom}}</a>
									<a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
									<!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
								<span><a href="#">{{$user->organisme}}</a></span>
							</div>
						</li>
						
						<li>
							<div class="profile-stat">
								<h3> <center><i class="entypo-mobile"></i></center></h3>
								<span><a href="#">{{$user->num_telephone}}</a></span>
							</div>
						</li>
						
						
					</ul>
					
				</div>
				
				<div class="col-sm-3">
					
					
				</div>
				
			</header>


       
			<section class="profile-info-tabs" style="margin-left:0px; margin-right: 0px;">
				
				<div class="row">
					
					<div class="col-sm-offset-2 col-sm-10">
						
						<ul class="user-details" style="font-size: 13px;">
							<li>
								<a href="#" style="color: #303641;">
									<i class="entypo-mail"></i>
									{{$user->email}}
								</a>
							</li>
							
							<li>
								<a href="#" style="color: #303641;">
									<i class="entypo-suitcase"></i>
									{{ __('messages.work') }} : <span><b> {{$user->organisme}} </b></span>
								</a>
							</li>
							
							<li>
								<a href="#" style="color: #303641;">
									<i class="entypo-calendar"></i>
									{{ __('messages.inscription_date') }} : {{$user->created_at->toDateString()}}
								</a>
							</li>
						</ul>
						
						
						<!-- tabs for the profile links -->
						
							<div class="row">

								<div class="col-md-9"></div>
								<div class="col-md-3">
									
									<button type="button" class="btn btn-primary btn-icon icon-left" data-toggle="modal" data-target="#createModal"><i class="entypo-info-circled" ></i> {{ __('messages.update') }}</button>
								</div>


							</div>
								
						
						
					</div>
					
				</div>
				
			</section>


    <div  class="row">
    	
        <div class="col-md-9">
        	 calendar
        </div>

        <div class="col-md-3">
      	
		<script type="text/javascript">
			jQuery(document).ready(function($)
			{
				var $todo_tasks = $("#todo_tasks");
				
				$todo_tasks.find('input[type="text"]').on('keydown', function(ev)
				{
					if(ev.keyCode == 13)
					{
						ev.preventDefault();
						
						if($.trim($(this).val()).length)
						{
							var $todo_entry = $('<li><div class="checkbox checkbox-replace color-white"><input type="checkbox" /><label>'+$(this).val()+'</label></div></li>');



							//alert($(this).val());
                             $.ajax({
								    type: 'POST',
								    url: '/task/add',
								    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
								    data: {task:$(this).val()},
								    success: function( msg ) {

								    $("#tasks").fadeIn("fast");	
								    $("#tasks").fadeout("fast");
								    
					               

								    }
								  });


							$(this).val('');
							$todo_entry.appendTo($todo_tasks.find('.todo-list'));
							$todo_entry.hide().slideDown('fast');

							
							replaceCheckboxes();
						}
					}
				});
			});
		</script>
		
		<div class="row" id="tasks">
			
			<div class="col-sm-12">
			
				<div class="tile-block tile-green" id="todo_tasks">
					
					<div class="tile-header">
						<i class="entypo-list"></i>
						
						<a href="#">
							Tasks
							<span>To do list, tick one.</span>
						</a>
					</div>
					
					<div class="tile-content">
						
						<input type="text" class="form-control" placeholder="Add Task" />
						
						
						<ul class="todo-list">
							
							@foreach($user_tasks as $task)

							<li>
								<div class="checkbox checkbox-replace color-white">
								    @if($task->etat=="non")
									<input type="checkbox" id="{{$task->id}}" onclick="change_etat({{$task->id}})" data-etat="non" />
                                    @else
                                    <input type="checkbox" id="{{$task->id}}" checked onclick="change_etat({{$task->id}})" data-etat="oui"/>	
									@endif
									<label>{{$task->name_task}}</label>
								</div>
							</li>
							
							@endforeach
							
						</ul>
						
					</div>
					
				</div>
				
			</div>
			
			
		
			
		</div>
		
		<br />



        </div>

    </div>			


</div>



<!--  ***********    Modal Create new group   **************** -->



  <div id="createModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

  		<!-- Modal content-->

  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal">&times;</button>
  				<h4 class="modal-title"><i class="entypo-plus"></i>  {{ __('messages.update') }}</h4>
  			</div>

  			<div class="modal-body">
  			<form class="form-horizontal" action="/user/update" method="post">
  					{{ csrf_field() }}


  		<div class="form-group" hidden="true">
            <label class="control-label col-sm-3" for="email">ID</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-user"></i></span>
              <input type="text" name="id_user" class="form-control typeahead" required="true" value="{{$user->id}}" />
              </div>
              </div>
            </div>


  					<div class="form-group">
  					<label class="control-label col-sm-3" for="email">{{ __('messages.first_name') }}</label>
  						<div class="col-sm-8">
  						  <div class="input-group">
							<span class="input-group-addon"><i class="entypo-user"></i></span>
              <input type="text" name="nom" class="form-control typeahead" required="true"  value="{{$user->prenom}}" />
						  </div>
  						</div>
  					</div>

            <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.family_name') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-user"></i></span>
              <input type="text" name="prenom" class="form-control typeahead" required="true" value="{{$user->nom}}" />
              </div>
              </div>
            </div>


            <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.organization') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-home"></i></span>
              <input type="text" name="organisme" class="form-control typeahead" required="true" value="{{$user->organisme}}" />
              </div>
              </div>
            </div>


          


              <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.phone') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-phone"></i></span>
              <input type="text" name="num_telephone" class="form-control typeahead" required="true" value="{{$user->num_telephone}}" />
              </div>
              </div>
            </div>

              <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.email') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-mail"></i></span>
              <input type="text" name="email" class="form-control typeahead" required="true"  value="{{$user->email}}" />
              </div>
              </div>
            </div>


           <div class="form-group">
            <label class="control-label col-sm-3" for="email"> old {{ __('messages.password') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-key"></i></span>
              <input type="password" name="old_password" class="form-control typeahead" required="true" />
              </div>
              </div>
            </div>


            <div class="form-group">
            <label class="control-label col-sm-3" for="email">{{ __('messages.password') }}</label>
              <div class="col-sm-8">
                <div class="input-group">
              <span class="input-group-addon"><i class="entypo-key"></i></span>
              <input type="password" name="new_password" class="form-control typeahead" required="true" />
              </div>
              </div>
            </div>

           




		  			<div class="modal-footer">

		  			  <button type="submit" class="btn btn-primary" >{{ __('messages.update') }}
		  			  </button>
		  			</div>

  			 </form>
  			</div>

  			
  		</div>


  	</div>
  </div>

<!--  ***********    END  Modal Create new group   **************** -->






@endsection('content')

  
  @section('js_files')
  <script src="{{ URL::to('js/neon-chat.js') }}"></script>
  @endsection('js_files')



   @section('js')

    <script type="text/javascript">
    	
    	 function change_etat(id_task){
      
             var etat;

    	 	 if($("#"+id_task).attr('data-etat')=="oui") {
                etat="non";
    	 	 } else {
               etat="oui";
    	 	 }
    

    	 	$.ajax({
			    type: 'POST',
			    url: '/task/update',
			    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			    data: {id:id_task,etat:etat },
			    success: function( msg ) {

			    $("#tasks").fadeIn("fast");	
			    $("#tasks").fadeout("fast");
			    
               

			    }
			  });

    	 	
    	 }


    </script>


   @endsection('js')