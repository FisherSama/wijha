 @extends('layouts.layout_admin')


 @section('content')

 <input type="text" id="id_grp" value="{{$id_groupe}}" hidden="true" />

 <div class="panel panel-primary" data-collapsed="0">
					
			<div class="panel-heading">
				<div class="panel-title">
					 <h3> {{$nom_groupe}}</h3>
				</div>
				
				<div class="panel-options">
					<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
					<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
				</div>
			</div>
			
			
			<div class="panel-body">
		
				<div id="list-1" class="nested-list dd with-margins"><!-- adding class "with-margins" will separate list items -->
               
				@foreach($all_menus as $menu)
					
					<ul class="dd-list">
						<li class="dd-item">
							<div class="dd-handle">
									<div class="row">
										<div class="col-md-8"> <h4> <b>
										   {{__('messages.'.$menu["nom_menu"])}}
										</b>
                                  
										 </h4></div>
										<!--<div class="col-md-3 ">
										  <div class="make-switch switch-mini">
											    <input type="checkbox" >
										   </div>
										</div>-->
								</div>	 
							</div>


							@foreach($menu['list'] as $fct)

							<ul class="dd-list" style="">
								<li class="dd-item">
									<div class="panel panel-primary" style=" padding: 8px; margin: 1px;/*pointer-events:none;*/">
										  <div class="row">
												<div class="col-md-8">{{__('messages.'.$fct['nom_fonctionnalite'])}} </div>
												<div class="col-md-3 ">
												  <div class="make-switch switch-mini">
												   @php
												   if(in_array($fct['nom_fonctionnalite'], $list_permission))
											        { @endphp
											          <input type="checkbox" checked="true" name="{{$fct['id']}}">
											        @php } else { @endphp
                                                      <input type="checkbox" name="{{$fct['id']}}">
											       @php  }
												   @endphp
													    
												   </div>
												</div>
								          </div>
									</div>
								</li>
							 </ul>

							 @endforeach


						</li>
					</ul>

           @endforeach


						
				</div>
          
                <br>
				<div class="row">
					 <div class="col-md-10"></div>
					 <div class="col-md-2">
					  <button type="button" class="btn btn-green btn-icon" onclick="update_permission()">
								{{__('messages.update')}}
								<i class="entypo-check"></i>
							</button>
					 </div>

				</div>
		
			</div>
		
		</div>


 @endsection('content')


 @section('js_files')

  <script src="{{ URL::to('js/jquery.nestable.js') }}"></script>
  <script src="{{ URL::to('js/bootstrap-switch.min.js') }}"></script>
  <script src="{{ URL::to('js/neon-chat.js') }}"></script>

  @endsection('js_files')






  @section('js')
    <script type="text/javascript">
		jQuery(document).ready(function($)
		{
			$('.dd').nestable({});
		});
    </script>

    <script type="text/javascript">

      var id_grp=$("#id_grp").val();
     

      function update_permission(){
       
        var selected = [];
			$('div#list-1 input[type=checkbox]').each(function() {
				var check = new Object();
				check.id=$(this).attr('name');

			   if ($(this).is(":checked")) {
			      // selected.push($(this).attr('name'));
			      check.etat="true";
			   } else{
                  check.etat="false";
			   }
			 selected.push(check);
		});


			$.ajax({
			    type: 'POST',
			    url: '/groups/update_permission/',
			    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			    data: {id_group:id_grp,list_choice:JSON.stringify(selected) },
			    success: function( msg ) {
			    
                location.reload();

			    }
			  });
     }


		
		   

    </script>
  @endsection('js')