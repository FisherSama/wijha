  @extends('layouts.layout_admin')
  @section('content')

 	
			
        
		<div class="calendar-env">
			
			<!-- Calendar Body -->
			<div class="calendar-body">
				
				<div id="calendar"></div>
				
				
			</div>
			
			<!-- Sidebar -->
			<div class="calendar-sidebar">
				
				<!-- new task form -->
				<div class="calendar-sidebar-row">
						
					<form role="form" id="add_event_form">
					
						<div class="input-group minimal">
							<input type="text" class="form-control" placeholder="select playlist" />


							
							<div class="input-group-addon">
								<i class="entypo-pencil"></i>
							</div>
						</div>
						
					</form>
					
				</div>
			
			
				<!-- Events List -->
				<ul class="events-list" id="draggable_events">
					
				</ul>
				
			</div>
			
		</div>

  @endsection('content')

  @section('js_files')

  
    <script src="{{ URL::to('js/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ URL::to('js/neon-calendar.js') }}"></script>


  @endsection('js_files')

