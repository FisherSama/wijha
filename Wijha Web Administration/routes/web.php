<?php
/* ------------------------------------------------------ Logins routes ---------------------------------------------------- */
Route::get('/dashboard', 'UserController@index')->name('dashboard');
Route::get('/', 'UserController@index')->name('dashboard');
Route::get('/home', 'UserController@index')->name('home');
Route::get('/register', function () {
    return view('register');
});

/*Route::get('/confirmIdentity', function () {
    return view('auth.register_1');
});*/

Route::get('/template', function () {
    return view('template');
});

Route::get('/templatee', function () {
    return view('media');
});

Auth::routes();

Route::get('/menu', 'MenuController@index')->name('menu');

Route::get('/fct', 'FonctionnaliteController@index')->name('fct');
Route::get('/usr_f', 'UserFonctController@index')->name('usr_f');

Route::get('/language/{lng}', 'Languages@index')->name('language');

/* -------------------------------------------------- routes Abonnement ---------------------------------------------------- */
Route::get('/plans', 'AbonnementController@index')->name('plans');
Route::post('/newAbonnement', 'AbonnementController@create')->name('newAbonnement');
Route::post('/editAbonnement', 'AbonnementController@edit')->name('editAbonnement');
Route::post('/deleteAbonnement', 'AbonnementController@delete')->name('deleteAbonnement');

Route::get('/ourAbonne', 'UserAbonController@ourAbonne')->name('ourAbonne');
Route::post('/allAbonne', 'UserAbonController@allAbonne')->name('allAbonne');
Route::post('/abonneByAbonnement', 'UserAbonController@abonneByAbonnement')->name('abonneByAbonnement');

Route::get('/monAbonnemen', 'UserAbonController@myAbonnement')->name('monAbonnemen');


Route::get('/aaab', 'UserAbonController@test');

/* ----------------------------------------------------- routes DemandeQuota ----------------------------------------------- */
Route::get('/requestAbonne', 'DemandeQuotaController@index')->name('requestAbonne');
Route::post('/alldemande', 'DemandeQuotaController@alldemande')->name('alldemande');
Route::post('/newDemande', 'DemandeQuotaController@newDemande')->name('newDemande');

Route::post('/demande/validation', 'DemandeQuotaController@validation');
Route::post('/demande/refuser', 'DemandeQuotaController@refuser');


Route::get('/aaa', 'DemandeQuotaController@test');

/* ------------------------------------------------------ routes Groupe ---------------------------------------------------- */

Route::get('/my_groups', 'GroupeController@index')->name('groups');
Route::post('/groups/add', 'GroupeController@addGroup')->name('groupsAdd');
Route::post('/groups/delete', 'GroupeController@deleteGroup')->name('groupsDelete');
Route::post('/groups/update', 'GroupeController@updateGroup')->name('groupsUpdate');
Route::get('/groups/consulte/{id_groupe}', 'GroupeController@membersGroupe')->name('membersGroup');
Route::get('/groups/members/', 'GroupeController@membersAllgroupsOfAdmin')->name('list_user');
Route::post('/user/add/', 'UserController@addSimpleUser')->name('add_user');
Route::post('/user/delete/', 'UserController@deleteUser')->name('delete_user');
Route::get('/groups/permission/{id_groupe}', 'UserFonctController@index')->name('permissionGroup');
Route::post('/groups/update_permission/', 'UserFonctController@updatePermission')->name('permissionGroup');
Route::get('/user/profil/', 'UserController@profil')->name('profil');
Route::post('/user/update', 'UserController@updateUser')->name('update_profil');

Route::post('/task/add', 'UserController@addTask')->name('add_task');
Route::post('/task/update', 'UserController@updateTask')->name('update_task');

Route::get('/layout', 'LayoutController@index');
Route::get('/plannification', 'PlanningController@index');

Route::get('/region/edit/{id_region}', 'RegionController@edit');
Route::get('/screen/add', 'ScreenController@index');
Route::get('/display', 'ScreenController@displays');










