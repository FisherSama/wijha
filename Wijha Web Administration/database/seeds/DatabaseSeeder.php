<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        /*DB::table('users')->insert([
            'nom' => str_random(10),
            'prenom' => str_random(10),
            'organisme' => str_random(10),
            'num_telephone' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'etat' => str_random(10),
            'role' => str_random(10),
        ]);*/

         DB::table('menus')->insert([
            'nom_menu' => "users",
            'type' => "not_superUser",
        ]);

        DB::table('menus')->insert([
            'nom_menu' => "abonnement",
            'type' => "not_superUser",
        ]);


         DB::table('fonctionnalites')->insert([
            'nom_fonctionnalite' => "users_group",
            'menu_id' => 1,
            'type' => "not_superUser",
        ]);

          DB::table('fonctionnalites')->insert([
            'nom_fonctionnalite' => "users_list",
            'menu_id' => 1,
            'type' => "not_superUser",
        ]);


        DB::table('fonctionnalites')->insert([
            'nom_fonctionnalite' => "nos_abonnements",
            'menu_id' => 2,
            'type' => "superUser",
        ]);

          DB::table('fonctionnalites')->insert([
            'nom_fonctionnalite' => "nos_abonnes",
            'menu_id' => 2,
            'type' => "superUser",
        ]);

        DB::table('fonctionnalites')->insert([
            'nom_fonctionnalite' => "requete_abonnes",
            'menu_id' => 2,
            'type' => "superUser",
        ]);

         DB::table('fonctionnalites')->insert([
            'nom_fonctionnalite' => "mon_abonnement",
            'menu_id' => 2,
            'type' => "superUser",
        ]);



         /* DB::table('abonnements')->insert([
            'titre_abonnement' => "mon_abonnement",
            'licence' => "licence test",
            'tarification' => "12000 DA",
            'quota' => "50 GO",
            'max_user' => 5,
            'duree' => 3,
         ]); 


            DB::table('user_abons')->insert([
            'user_id  ' => "mon_abonnement",
            'abonnement_id ' => "licence test",
            'tarification' => "12000 DA",
            'quota' => "50 GO",
            'max_user' => 5,
            'duree' => 3,
         ]);*/


         /*DB::table('user_foncts')->insert([
            'groupe_id' => 1,
            'fonctionnalite_id' => 2,
        ]);*/
    }
}
