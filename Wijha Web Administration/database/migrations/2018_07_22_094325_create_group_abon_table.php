<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupAbonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_abon', function (Blueprint $table) {
           
            $table->bigInteger('groupe_id')->unsigned();
            $table->bigInteger('abonnement_id')->unsigned();


             $table->foreign('groupe_id')->references('id')->on('groupes')->onDelete('cascade');

            $table->foreign('abonnement_id')->references('id')->on('abonnements')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_abon');
    }
}
