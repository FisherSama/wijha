<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAbonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_abons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('abonnement_id')->unsigned();

            $table->string('titre')->nullable();

            $table->date('date_debut');
            $table->date('date_fin');
            $table->double('quota_disponible');
            $table->double('nbr_afficheur_dispo');
            $table->double('nbr_licence_dispo');
            $table->integer('nbr_user_disponible');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('abonnement_id')->references('id')->on('abonnements')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_abons');
    }
}
