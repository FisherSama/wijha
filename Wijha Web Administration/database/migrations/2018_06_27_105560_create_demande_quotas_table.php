<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandeQuotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demande_quotas', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('user_abons_id')->unsigned();
            $table->foreign('user_abons_id')->references('id')->on('user_abons')->onDelete('cascade');

            $table->string('numero_demande');
            $table->string('titre_demande');
            
            $table->string('etat_quota')->default('En attente');
            $table->integer('nbr_user')->default(0);
            $table->integer('nbr_afficheur')->default(0);
            $table->integer('nbr_licence')->default(0);
            $table->integer('espace_demander');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demande_quotas');
    }
}
