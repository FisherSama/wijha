package com.hajjhackathon.wijha.wijhasmarttv;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    FrameLayout frameLayout;
    String link = "https://gist.githubusercontent.com/OusamaLaidi/e2ce248c1c13ac67d2f37573b3fa4553/raw/87c1573fcba3a82d34ac7bc90a5d205df56659ca/region.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = (FrameLayout)findViewById(R.id.frameLayout);
        new dispProj().execute();

    }

    public class dispProj extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            /*** Data Request ***/
            String result = null;
            URL url = null;
            try {
                url = new URL(link);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = inputStreamToString(in);
                Log.e("Json",result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                /*********** Create Layout ************/
                JSONObject layout = jsonObject.getJSONObject("layout");
                int height = Integer.parseInt(layout.getString("height"));
                int width = Integer.parseInt(layout.getString("width"));
                String background = layout.getString("background");
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                frameLayout.setBackgroundColor(Color.BLACK);
                if (width>height){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                else if (height<width){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                }

                /********* Regions *******/
                JSONArray region = null;
                region = jsonObject.getJSONArray("regions");
                for (int i = 0; i < region.length(); i++) {
                    try {
                        JSONObject json_data = region.getJSONObject(i);
                        int height_view = Integer.parseInt(json_data.getString("height"));
                        int width_view = Integer.parseInt(json_data.getString("width"));
                        int top_view = Integer.parseInt(json_data.getString("top"));
                        int left_view = Integer.parseInt(json_data.getString("left"));

                        /**** Media in region ***/
                        JSONObject media = json_data.getJSONObject("media");
                        String type_media = media.getString("type");
                        String url = media.getString("url");
                        switch (type_media)
                        {
                            case "image":
                                ImageView imageView = new ImageView(MainActivity.this);
                                imageView.requestLayout();
                                FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(height_view, width_view);
                                lp.setMargins(left_view,top_view,0,0);
                                imageView.setLayoutParams(lp);
                                frameLayout.addView(imageView);
//                                Glide.with(imageView.getContext())
//                                        .load(url)
//                                        .into(imageView);
                                break;
                            case "video":
                                VideoView videoView = new VideoView(MainActivity.this);
                                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mp.setLooping(true);
                                    }
                                });
                                FrameLayout.LayoutParams lp2 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                lp2.setMargins(0,0,0,0);
                                videoView.setLayoutParams(lp2);
                                videoView.setVideoURI(Uri.parse(url));
                                videoView.start();
                                frameLayout.addView(videoView);

                                break;
                            default:
                                Log.e("Error:","Unexpected Type !");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Toast.makeText(MainActivity.this, "Layout Created with" + height + "x" + width, Toast.LENGTH_SHORT).show();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader rd = new BufferedReader(isr);

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }
}
